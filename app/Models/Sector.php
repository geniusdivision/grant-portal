<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    public static function dropdownOptions(){
        $sectors = self::where('parent_id',0)->orderBy('eb_code')->get();

        $results = [];
        foreach($sectors as $sector)
        {
            $subsectors = self::where('parent_id',$sector->id)->orderBy('name')->get();
            $results[] = [
                "name" => $sector->name,
                "eb_code" => $sector->eb_code,
                "subsectors" => $subsectors,
            ];
        }

        return $results;
    }
}

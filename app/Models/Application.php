<?php

namespace App\Models;

use App\Http\Traits\Hashidable;
use App\Notifications\ApplicationRejectedNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;
    use Hashidable;

    protected $guarded = ['id'];

    protected $dates = ['submitted_at','resubmitted_at'];

    public static function boot() {
        parent::boot();

        static::deleting(function($application) {
//            if($application->isForceDeleting())
//            {


                $application->applicationResponses()->delete();
                $application->applicationNotes()->delete();
            //}
        });


    }

    public function applicationForm(){
        return $this->belongsTo('App\Models\ApplicationForm');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function applicationNotes(){
        return $this->hasMany('App\Models\ApplicationNote');
    }

    public function applicationResponses(){
        return $this->hasMany('App\Models\ApplicationResponse');
    }

    public function projectOfficers(){
        return $this->belongsToMany('App\Models\User','application_officer','application_id','user_id')->using('App\Models\Pivots\AssignmentsPivot');
    }

    public function getReferenceAttribute(){
        return "BMBC-" . strtoupper($this->applicationForm->slug) . "-" . str_pad($this->id,4,"0",STR_PAD_LEFT);
    }

    public function scopeInReview($query)
    {
        return $query->where('status', 'In review');
    }

    public function scopeMoreInformationRequired($query)
    {
        return $query->where('status', 'More information required');
    }

    public function getID(){
        //TODO: Replace with hash
        return $this->id;
    }

    public static function color($status){
        $colors = [
            'In progress' => 'indigo',
            'Submitted' => 'green',
            'In review' => 'yellow',
            'More information required' => 'yellow',
            'Rejected' => 'red',
            'Accepted' => 'green',
        ];

        return $colors[$status] ?? 'indigo';
    }

    public function getField($stepKey,$key)
    {
        $response = ApplicationResponse::where([
            'application_id' => $this->id,
            'step' => $stepKey,
        ])->first();
        if(!$response)
            return null;
        return $response->values[$key] ?? null;
    }

    public function getCompanyNameAttribute(){
        return $this->getField('applicant-details','businessName');
    }

    public function percentComplete(){
        $count = ApplicationResponse::where([
            'application_id' => $this->id,
        ])->count();

        return $count ? ($count/4)*100 : 0;
    }

    public function reject($notify = true){
        if($this->status != "Rejected")
        {
            $this->update([
                'status' => 'Rejected',
                'current_step' => 1,
            ]);
            if($notify)
                $this->user->notify(new ApplicationRejectedNotification($this));
        }
    }

    public function setStatus($newStatus){
        $this->update([
            'status' => $newStatus,
            'current_step' => 1,
        ]);
    }
}

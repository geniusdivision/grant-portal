<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class ApplicationForm extends Model
{
    use HasFactory;
    use HasSlug;

    protected $guarded = ['id'];

    protected $dates = ['submission_period_end'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function applications(){
        return $this->hasMany('App\Models\Application');
    }

    public function applicationWindowClosed(){
        return $this->submission_period_end !== null && $this->submission_period_end <= Carbon::now();
    }
}

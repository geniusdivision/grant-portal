<?php

namespace App\Models\Pivots;

use App\Events\ApplicationAssigned;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AssignmentsPivot extends Pivot
{
    protected $dispatchesEvents = [
        'created' => ApplicationAssigned::class
    ];
}

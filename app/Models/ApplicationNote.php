<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationNote extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function application(){
        return $this->belongsTo('App\Models\Application');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}

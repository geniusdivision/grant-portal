<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationResponse extends Model
{
    use HasFactory;

    protected $casts = [
        'values' => AsArrayObject::class,
    ];
}

<?php

namespace App\Listeners;

use App\Events\ApplicationAssigned;
use App\Models\Application;
use App\Notifications\InReviewNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ApplicationAssignedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ApplicationAssigned $event)
    {

        $application = Application::find($event->pivot->application_id);

        if($application->projectOfficers->count() == 1)
        {
            $application->status = "In review";
            $application->user->notify(new InReviewNotification($application));
            $application->save();
        }
    }
}

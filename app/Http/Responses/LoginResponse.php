<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {

        if($request->wantsJson())
        {
            return response()->json(['two_factor' => false]);
        }
        else
        {
            if(Auth::user()->can('review applications'))
                return redirect(route('assignments'));
            elseif(Auth::user()->can('create applications'))
                return redirect(route('applications'));
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    public function index(){
        $applications = Auth::user()->applications;
        $title = "Your applications";

        return view('applications.index')->with([
            'applications' => $applications,
            'title' => $title,
        ]);
    }

    public function assignments(){
        $inReview = Application::whereHas('projectOfficers',function($q){
            $q->where('id',Auth::user()->id);
        })->where('status','In review')->get();
        $moreInformationRequired = Application::whereHas('projectOfficers',function($q){
            $q->where('id',Auth::user()->id);
        })->where('status','More information required')->get();

        return view('assignments.index')->with([
            'inReview' => $inReview,
            'moreInformationRequired' => $moreInformationRequired,
        ]);
    }

}

<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FormHeader extends Component
{
    public $application;
    public $currentStep;
    public $totalSteps;
    public $navEnabled = true;

    public function onNavigate($index){
        $this->emitUp('onStep',$index);
    }

    public function render()
    {
        return view('livewire.form-header');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductivityGrantApplicationForm extends Component
{
    public $name = "Productivity Grant";

    public $currentStep = 1;

    public $steps = [
        'details',
        'productivity-challenge',
        'measure-productivity',
        'improve-productivity',
        'impacts',
    ];

    public $productivityImpactForm = [
        "impact" => "",
        "pre" => "",
        "post" => "",
        "timescale" => "",
    ];

    public function mount(){
        $this->currentStep = env("FORM_STEP") ?? 1;
    }

    public $productivityImpact = [
        [
            "impact" => "Example - Hours worked on support/hosting clients per month",
            "pre" => "98",
            "post" => 89,
            "timescale" => "2022",
        ],
        [

            "impact" => "Example - Productivity per hour (based on 2019/20 average)",
            "pre" => "£55.88",
            "post" => "£61.47",
            "timescale" => "2022",
        ],
    ];

    public $economicImpact = [
        [
            "impact" => "Jobs Created (FTEs)",
            "value" => "1",
            "percent_increase" => 20,
            "timescale" => "2022",
        ],
        [
            "impact" => "Jobs Safeguarded",
            "value" => "4.5",
            "percent_increase" => "n/a",
            "timescale" => "2022",
        ],
    ];

    public function render()
    {
        return view('livewire.forms.productivity-grant.' . $this->steps[$this->currentStep-1]);
    }

    public function onPrev(){
        $this->currentStep--;
    }

    public function onNext(){
        $this->currentStep++;
    }

    public function onAddProductivityImpact(){
        $this->validate([
            'productivityImpactForm.impact' => 'required|string',
            'productivityImpactForm.pre' => 'required|string',
            'productivityImpactForm.post' => 'required|string',
            'productivityImpactForm.timescale' => 'required|string',
        ],[
           'required' => "This field is required."
        ]);
        $this->productivityImpact[] = [
            "impact" => $this->productivityImpactForm['impact'],
            "pre" => $this->productivityImpactForm['pre'],
            "post" => $this->productivityImpactForm['post'],
            "timescale" => $this->productivityImpactForm['timescale'],
        ];

        $this->productivityImpactForm = [
            "impact" => "",
            "pre" => "",
            "post" => "",
            "timescale" => "",
        ];
    }

    public function onRemoveProductivityImpact($index)
    {
        unset($this->productivityImpact[$index]);
    }
}

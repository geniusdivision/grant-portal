<?php

namespace App\Http\Livewire\Forms;

use App\Models\Application;
use App\Models\ApplicationForm;
use App\Models\ApplicationResponse;
use App\Models\User;
use App\Notifications\ApplicationAcceptedNotification;
use App\Notifications\ApplicationRejectedNotification;
use App\Notifications\ApplicationResubmittedNotification;
use App\Notifications\ApplicationSubmittedNotification;
use App\Notifications\MoreInformationRequiredNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Livewire\Component;

class DIGC extends Component
{
    public $application;
    public $name;
    public $applicationResponse;
    public $reviewing;
    public $disabled;
    public $currentStep;
    public $stepKey;


    public function mount(Application $application, $reviewing = false,$disabled = false){
        $this->application = $application;
        $this->name = $this->application->applicationForm->name;
        $this->currentStep = $application->current_step;
        $this->loadApplicationResponse();

        $this->reviewing = $reviewing;
        if($this->reviewing)
        {
            $this->disabled = true;
        }
        if($disabled)
            $this->disabled = true;
        if($this->application->status == "In review" || $this->application->status == "Accepted" || $this->application->status == "Rejected")
        {
            if(Auth::user()->can('create applications'))
                $this->banner('This application has the status ' . $this->application->status  . ' and cannot be edited');
            $this->disabled = true;
        }
        if($this->application->status == "In progress" && $this->application->applicationForm->applicationWindowClosed())
        {
            if(Auth::user()->can('create applications'))
                $this->banner('The application window has now closed; this application cannot currently be submitted. <a x-on:click="show = false" class="underline" href="' . route('applications.closed',$this->application->applicationForm) . '">More information</a>','danger');
        }
    }

    protected $listeners = ['onStep' => 'onStep'];

    public function onStep($index)
    {
        if($this->application->status != "In progress")
        {
            $this->currentStep = $index;
            $this->loadApplicationResponse();
        }
    }

    public $steps = [
        'applicant-details' => [
            "fields" => [
                "businessName" => null,
                "companyPosition" => null,
                "address1" => null,
                "address2" => null,
                "city" => null,
                "postcode" => null,
                "telephone" => null,
                "mobile" => null,
                "email" => null,
                "website" => null,
                "businessSize" => "SME",
                "businessType" => "Limited Company",
                "tradingStartDate" => null,
                "socialEnterprise" => 0,
                "localEmployees" => 0,
                "nationalEmployees" => 0,
                "globalEmployees" => 0,
                "companyNumber" => null,
                "soleTraderUTR" => null,
                "businessSector" => 2,
                "registeredLocation" => "England & Wales",
                "vatNumber" => null,
                //"registeredDisability" => ,
                "projectStartDate" => null,
                "projectEndDate" => null,
                "projectCost" => 0,
                "grantRequested" => 0,
                "proposedSupplier" => null,
                "aboutProject" => null,
            ]
        ],
        'assessment' => [
            "fields" => [
                "businessProblem" => null,
                "digitalTransformation" => null,
                "economicImpact" => null,

                "jobsCreatedValue" => null,
                "jobsCreatedIncrease" => null,
                "jobsCreatedTimescale" => null,

                "jobsSafeguardedValue" => null,
                "jobsSafeguardedIncrease" => null,
                "jobsSafeguardedTimescale" => null,

                "increasedTurnoverValue" => null,
                "increasedTurnoverIncrease" => null,
                "increasedTurnoverTimescale" => null,

                "increasedProfitValue" => null,
                "increasedProfitIncrease" => null,
                "increasedProfitTimescale" => null,

                "chosenSupplier" => null,
            ],
        ],
//        'previous-aid' => [
//            "fields" => [],
//        ],
        'confirmation' => [
            "fields" => [
                "privacy" => null,
                "data" => null,
                "authorised" => null,
                "terms" => null,
                "future_participation_not_required" => null,
                "document_retention" => null,
                "accuracy" => null,
            ],
        ],
        //'sap-vendor-account',
//        'business-problem',
//        'digital-transformation',
//        'economic impacts',
//        'supplier-rationale',
    ];

    private function grantRequestedMax(){
        if($this->steps[$this->stepKey]['fields']["projectCost"] >= 10000)
            return 5000;
        return ($this->steps[$this->stepKey]['fields']["projectCost"]/2);

    }

    public function rules(): array
    {
        if($this->currentStep == 1)
        {
            $rules = [
                'steps.'.$this->stepKey.'.fields.businessName' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.companyPosition' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.address1' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.address2' => ['nullable','string'],
                'steps.'.$this->stepKey.'.fields.city' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.postcode' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.telephone' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.mobile' => ['nullable','string'],
                'steps.'.$this->stepKey.'.fields.email' => ['required','email'],
                'steps.'.$this->stepKey.'.fields.website' => ['nullable','string'],
                'steps.'.$this->stepKey.'.fields.businessSize' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.businessType' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.tradingStartDate' => ['required','date_format:d/m/Y'],
                'steps.'.$this->stepKey.'.fields.socialEnterprise' => ['nullable','numeric'],
                'steps.'.$this->stepKey.'.fields.soleTraderUTR' => ['requiredIf:steps.'.$this->stepKey.'.fields.businessType,Self Employed'],
                'steps.'.$this->stepKey.'.fields.companyNumber' => ['requiredIf:steps.'.$this->stepKey.'.fields.businessType,Limited Company,Partnership,Charity'],
                'steps.'.$this->stepKey.'.fields.registeredLocation' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.vatNumber' => ['nullable','string','regex:/^(GB)([0-9]{9})$/i'],

                'steps.'.$this->stepKey.'.fields.localEmployees' => ['required','numeric'],
                'steps.'.$this->stepKey.'.fields.nationalEmployees' => ['required','numeric'],
                'steps.'.$this->stepKey.'.fields.globalEmployees' => ['required','numeric'],
                //'registeredDisability' => ['required','string'],
                'directors' => ['requiredIf:businessType,Limited Company,Partnership,Charity','array'],

                'steps.'.$this->stepKey.'.fields.projectStartDate' => ['required','date_format:d/m/Y'],
                'steps.'.$this->stepKey.'.fields.projectEndDate' => ['required','date_format:d/m/Y'],
                'steps.'.$this->stepKey.'.fields.projectCost' => ['required','numeric','max:10000'],
                'steps.'.$this->stepKey.'.fields.grantRequested' => ['required','numeric','max:'.$this->grantRequestedMax()],
                'steps.'.$this->stepKey.'.fields.proposedSupplier' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.aboutProject' => ['required','string'],
            ];

            if($this->steps[$this->stepKey]['fields']['businessType'] != 'Self Employed') {
                $rules ['directors'] = ['requiredIf:businessType,Limited Company,Partnership,Charity','array', 'min:1'];
            }
            return $rules;
        }
        elseif ($this->currentStep == 2)
        {
            return [
                'steps.'.$this->stepKey.'.fields.businessProblem' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.digitalTransformation' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.economicImpact' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.jobsCreatedValue' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.jobsCreatedIncrease' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.jobsCreatedTimescale' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.jobsSafeguardedValue' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.jobsSafeguardedIncrease' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.jobsSafeguardedTimescale' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.increasedTurnoverValue' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.increasedTurnoverIncrease' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.increasedTurnoverTimescale' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.increasedProfitValue' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.increasedProfitIncrease' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.increasedProfitTimescale' => ['required','string'],
                'steps.'.$this->stepKey.'.fields.chosenSupplier' => ['required','string'],
            ];
        }
        elseif ($this->currentStep == 3)
        {
            return [
                'steps.'.$this->stepKey.'.fields.privacy' => 'accepted',
                'steps.'.$this->stepKey.'.fields.data' => 'accepted',
                'steps.'.$this->stepKey.'.fields.authorised' => 'accepted',
                'steps.'.$this->stepKey.'.fields.terms' => 'accepted',
                'steps.'.$this->stepKey.'.fields.future_participation_not_required' => 'accepted',
                'steps.'.$this->stepKey.'.fields.document_retention' => 'accepted',
                'steps.'.$this->stepKey.'.fields.accuracy' => 'accepted',
            ];
        }
        return [];
    }

    public function messages(){
        if($this->currentStep == 1)
        {
            return [
                "date_format" => "Please enter a valid date in the format dd/mm/yyyy.",
                "required" => "This field is required.",
                "email" => "Please enter a valid email address.",
                "steps.$this->stepKey.fields.companyNumber.required_if" => "Please provide your " . $this->steps[$this->stepKey]['fields']["businessType"] . " number",
                "steps.$this->stepKey.fields.soleTraderUTR.required_if" => "The Sole Trader UTR field is required when Business Type is :value.",
                "directors.required_if" => "Please complete this section.",
                "directors.min" => "Please complete this section.",
                "steps.$this->stepKey.fields.aboutProject.required" => "Please provide a short summary of your proposed project",
                "steps.$this->stepKey.fields.projectCost.max" => "The total project cost must not be greater than £:max.",
                "steps.$this->stepKey.fields.grantRequested.max" => "The total grant requested must not be greater than £:max.",
                "steps.$this->stepKey.fields.vatNumber.regex" => "Your UK VAT number should start with GB, followed by nine digits.",
            ];
        }
        elseif($this->currentStep == 3)
        {
            return [
                "steps.$this->stepKey.fields.privacy.accepted" => 'Please confirm.',
                "steps.$this->stepKey.fields.data.accepted" => 'Please confirm.',
                "steps.$this->stepKey.fields.authorised.accepted" => 'Please confirm.',
                "steps.$this->stepKey.fields.terms.accepted" => 'Please confirm.',
                "steps.$this->stepKey.fields.future_participation_not_required.accepted" => 'Please confirm.',
                "steps.$this->stepKey.fields.document_retention.accepted" => 'Please confirm.',
                "steps.$this->stepKey.fields.accuracy.accepted" => 'Please confirm.',
            ];
        }
        return [
            "required" => "This field is required.",
        ];

    }



    public $directorFields = [
        "name" => "",
        "position" => "",
    ];

    public $directors = [

    ];

    public $previousAidFields = [
        "date" => "",
        "body" => "",
        "amount" => "",
        "purpose" => "",
    ];

    public $previousAid = [

    ];

    public function render()
    {
        return view('livewire.forms.' . $this->application->applicationForm->slug . "." . $this->stepKey);
    }

    public function onPrev(){
        $this->currentStep--;
        if(!$this->disabled)
        {
            $this->application->current_step = $this->currentStep;
            $this->application->save();
        }

        $this->loadApplicationResponse();
    }

    public function onNext(){
        if($this->disabled)
        {
            $this->currentStep++;
//            $this->application->current_step = $this->currentStep;
//            $this->application->save();
        }
        else
        {
            $rules = $this->rules();
            if(count($rules))
                $this->validate();

            if(!$this->applicationResponse)
            {
                $this->applicationResponse = new ApplicationResponse();
                $this->applicationResponse->application_id = $this->application->id;
                $this->applicationResponse->step = $this->stepKey;
            }
            //Update response values
            $vals = $this->steps[$this->stepKey]['fields'];
            if($this->currentStep == 1)
            {
                $vals['directors'] = $this->directors;
                $vals['previousAid'] = $this->previousAid;
            }
            $this->applicationResponse->values = $vals;
            $this->applicationResponse->save();

            $this->currentStep++;
            $this->application->current_step = $this->currentStep;
            $this->application->save();


        }

        $this->loadApplicationResponse();
    }

    public function onSubmit()
    {
        if($this->application->applicationForm->applicationWindowClosed())
            return;
        $rules = $this->rules();
        if (count($rules))
            $this->validate();

        if(!$this->applicationResponse)
        {
            $this->applicationResponse = new ApplicationResponse();
            $this->applicationResponse->application_id = $this->application->id;
            $this->applicationResponse->step = $this->stepKey;
        }
        //Update response values
        $vals = $this->steps[$this->stepKey]['fields'];
        $this->applicationResponse->values = $vals;
        $this->applicationResponse->save();

        $this->application->status = "Submitted";
        $this->application->submitted_at = Carbon::now();
        $this->application->current_step = 1;
        $this->application->save();

        foreach(User::whereHas("roles", function($q){ $q->where("name", "Manager"); })->get() as $manager)
        {
            $manager->notify(new ApplicationSubmittedNotification($this->application));
        }

        $this->banner('Thank you for submitting your application. A member of our team will be in touch with you soon.');

        return redirect()->to(route('applications'));
    }

    public function onResubmit()
    {
        $rules = $this->rules();
        if (count($rules))
            $this->validate();

        $vals = $this->steps[$this->stepKey]['fields'];
        $this->applicationResponse->values = $vals;
        $this->applicationResponse->save();

        $this->application->status = "In review";
        $this->application->resubmitted_at = Carbon::now();
        $this->application->current_step = 1;
        $this->application->save();

        foreach($this->application->projectOfficers as $projectOfficer)
        {
            $projectOfficer->notify(new ApplicationResubmittedNotification($this->application));
        }

        $this->banner('Thank you for submitting your application. A member of our team will be in touch with you soon.');

        return redirect()->to(route('applications'));
    }

    public function onMoreInformationRequired(){
        $this->application->status = "More information required";
        $this->application->current_step = 1;
        $this->application->save();

        $this->application->user->notify(new MoreInformationRequiredNotification($this->application));

        $this->banner('Application marked as More information required. An email has been sent to '.$this->application->user->email);

        return redirect()->to(route('assignments'));
    }

    public function onAccept(){
        //TODO: Anything else on accepted
        $this->application->status = "Accepted";
        $this->application->current_step = 1;
        $this->application->save();

        $this->application->user->notify(new ApplicationAcceptedNotification($this->application));

        $this->banner('Application marked as Accepted. An email has been sent to '.$this->application->user->email);

        return redirect()->to(route('assignments'));
    }

    public function onReject(){
        $this->application->reject();

        $this->banner('Application marked as Rejected. An email has been sent to '.$this->application->user->email);

        return redirect()->to(route('assignments'));
    }

    public function banner(string $message, string $style = 'success')
    {
        request()->session()->flash('flash.banner', $message);
        request()->session()->flash('flash.bannerStyle', $style);
    }

    public function loadApplicationResponse(){
        $this->stepKey = $this->stepKey = array_keys($this->steps)[$this->currentStep-1];
        $this->applicationResponse = ApplicationResponse::where([
            'application_id' => $this->application->id,
            'step' => $this->stepKey,
        ])->first();

        if($this->applicationResponse)
        {
            foreach($this->steps[$this->stepKey]['fields'] as $key => $field)
            {
                if(isset($this->applicationResponse->values[$key]))
                    $this->steps[$this->stepKey]['fields'][$key] = $this->applicationResponse->values[$key];
            }
            if($this->currentStep == 1)
            {
                if(isset($this->applicationResponse->values['directors']))
                {
                    $this->directors = $this->applicationResponse->values['directors'];
                }

                if(isset($this->applicationResponse->values['previousAid']))
                {
                    $this->previousAid = $this->applicationResponse->values['previousAid'];
                }
            }
            if($this->currentStep == 2)
            {
                //Backwards compatible
                if(isset($this->applicationResponse->values['economicImpactValues']))
                {
                    foreach($this->applicationResponse->values['economicImpactValues'] as $economicImpactValue)
                    {
                        if($economicImpactValue['impact'] == "Jobs created (FTE)")
                        {
                            $this->steps[$this->stepKey]['fields']['jobsCreatedValue'] = $economicImpactValue['value'];
                            $this->steps[$this->stepKey]['fields']['jobsCreatedIncrease'] = $economicImpactValue['increase'];
                            $this->steps[$this->stepKey]['fields']['jobsCreatedTimescale'] = $economicImpactValue['timescale'];
                        }
                        elseif ($economicImpactValue['impact'] == "Jobs safeguarded")
                        {
                            $this->steps[$this->stepKey]['fields']['jobsSafeguardedValue'] = $economicImpactValue['value'];
                            $this->steps[$this->stepKey]['fields']['jobsSafeguardedIncrease'] = $economicImpactValue['increase'];
                            $this->steps[$this->stepKey]['fields']['jobsSafeguardedTimescale'] = $economicImpactValue['timescale'];
                        }
                        elseif ($economicImpactValue['impact'] == "Increased Turnover")
                        {
                            $this->steps[$this->stepKey]['fields']['increasedTurnoverValue'] = $economicImpactValue['value'];
                            $this->steps[$this->stepKey]['fields']['increasedTurnoverIncrease'] = $economicImpactValue['increase'];
                            $this->steps[$this->stepKey]['fields']['increasedTurnoverTimescale'] = $economicImpactValue['timescale'];
                        }
                        elseif ($economicImpactValue['impact'] == "Increased Profit")
                        {
                            $this->steps[$this->stepKey]['fields']['increasedProfitsValue'] = $economicImpactValue['value'];
                            $this->steps[$this->stepKey]['fields']['increasedProfitsIncrease'] = $economicImpactValue['increase'];
                            $this->steps[$this->stepKey]['fields']['increasedProfitsTimescale'] = $economicImpactValue['timescale'];
                        }
                    }
                }
            }
        }
    }

    public function onAddDirector(){
        $this->validate([
            'directorFields.name' => 'required|string',
            'directorFields.position' => 'required|string',
        ],[
            'required' => "This field is required."
        ]);
        $this->directors[] = [
            "name" => $this->directorFields["name"],
            "position" => $this->directorFields["position"],
        ];
        $this->directorFields = [
            "name" => "",
            "position" => "",
        ];
    }

    public function onRemoveDirector($index){
        unset($this->directors[$index]);
    }

    public function onAddAid(){
        $this->validate([
            'previousAidFields.date' => 'required|string|date_format:m/Y',
            'previousAidFields.body' => 'required|string',
            'previousAidFields.amount' => 'required|numeric',
            'previousAidFields.purpose' => 'required|string',
        ],[
            'required' => "This field is required.",
            'date_format' => "Please enter a valid date in the format mm/yyyy."
        ]);
        $this->previousAid[] = [
            "date" => $this->previousAidFields['date'],
            "body" => $this->previousAidFields['body'],
            "amount" => $this->previousAidFields['amount'],
            "purpose" => $this->previousAidFields['purpose'],
        ];
        $this->previousAidFields = [
            "date" => "",
            "body" => "",
            "amount" => "",
            "purpose" => "",
        ];
    }

    public function onRemoveAid($index){
        unset($this->previousAid[$index]);
    }

    public function onAddEconomicImpact(){
        $this->validate([
            'economicImpactFields.impact' => 'required|string',
            'economicImpactFields.value' => 'required|string',
//            'economicImpactFields.increase' => 'required|string',
            'economicImpactFields.timescale' => 'required|string',
        ],[
            'required' => "This field is required."
        ]);
        $this->economicImpact[] = [
            "impact" => $this->economicImpactFields['impact'],
            "value" => $this->economicImpactFields['value'],
            "increase" => $this->economicImpactFields['increase'],
            "timescale" => $this->economicImpactFields['timescale'],
        ];

        $this->economicImpactFields = [
            "impact" => "",
            "value" => "",
            "increase" => "",
            "timescale" => "",
        ];
    }

    public function onRemoveEconomicImpact($index)
    {
        unset($this->economicImpact[$index]);
    }
}

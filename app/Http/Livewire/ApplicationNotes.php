<?php

namespace App\Http\Livewire;

use App\Models\ApplicationNote;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ApplicationNotes extends Component
{
    public $key;
    public $application;
    public $notes;

    public $noteText;

    public function mount($application,$key){
        $this->application = $application;
        $this->key = $key;
        $this->loadNotes();
    }

    public function loadNotes(){
        $this->notes = ApplicationNote::where([
            'key' => $this->key,
            'application_id' => $this->application->id,
        ])->get();
    }

    public function getShowNotesProperty(){
        if($this->application->status == "More information required")
            return true;
        if($this->application->status == "In review" && Auth::user()->can('review applications'))
            return true;

        return false;
    }

    public function render()
    {
        return view('livewire.application-notes');
    }

    public function onAddNote(){
        $this->validate([
            'noteText' => 'required'
        ],[
            'noteText.required' => 'Please enter a note.'
        ]);

        $note = ApplicationNote::create([
            'key' => $this->key,
            'application_id' => $this->application->id,
            'user_id' => Auth::user()->id,
            'note' => $this->noteText,
        ]);
        $this->noteText = null;
        $this->loadNotes();
    }
}

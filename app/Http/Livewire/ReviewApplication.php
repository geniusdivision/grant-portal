<?php

namespace App\Http\Livewire;

use App\Models\Application;
use Livewire\Component;

class ReviewApplication extends Component
{
    public $application;

    public function mount(Application $application)
    {
        $this->application = $application;
    }

    public function render()
    {
        return view('livewire.review-application');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Application;
use App\Models\ApplicationForm;
use App\Notifications\ApplicationStartedNotification;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Eligibility extends Component
{
    public $applicationForm;

    public function mount(ApplicationForm $form){
        $this->applicationForm = $form;
    }

    public $businessRates = [];
    public $barnsleyEmployeePromiseInitiative;
    public $businessSize;
    public $businessAge;
    public $businessToBusiness;
    public $financeAvailable;
    public $financialViability;
    public $restrictedSectors;
    public $stateAid;
    public $ebFunding;

    public function rules(){
        $rules = [
            "businessRates" => "required|array|min:1"
        ];
        if(in_array("Barnsley",$this->businessRates))
        {
            $rules["barnsleyEmployeePromiseInitiative"] = "required";
        }
        $rules["businessSize"] = "required";
        $rules["businessAge"] = "required";
        $rules["businessToBusiness"] = "required";
        $rules["financeAvailable"] = "required";
        $rules["financialViability"] = "required";
        $rules["restrictedSectors"] = "required";
        $rules["stateAid"] = "required";
        $rules["ebFunding"] = "required";
        return $rules;
    }

    public function onProceed()
    {
        $this->validate();

        //TODO: Select

        $app = Application::create([
            'user_id' => Auth::user()->id,
            'application_form_id' => $this->applicationForm->id,
            'status' => 'In progress'
        ]);

        //TODO: Prefill fields from profile

        Auth::user()->notify(new ApplicationStartedNotification($app));

        return redirect()->to(route('applications.apply',["form"=>$this->applicationForm,"application"=>$app]));
    }

    public function render()
    {
        return view('livewire.eligibility');
    }
}

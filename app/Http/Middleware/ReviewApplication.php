<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewApplication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $application =  $request->route('application');
        if (!$application->projectOfficers->contains(Auth::user()->id)) {
            abort(403, 'Unauthorized action.');
        }
        return $next($request);
    }
}

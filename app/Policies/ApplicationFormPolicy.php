<?php

namespace App\Policies;

use App\Models\ApplicationForm;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationFormPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can("assign applications");
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApplicationForm  $applicationForm
     * @return mixed
     */
    public function view(User $user, ApplicationForm $applicationForm)
    {
        return $user->can("assign applications");
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApplicationForm  $applicationForm
     * @return mixed
     */
    public function update(User $user, ApplicationForm $applicationForm)
    {
        return $user->can("assign applications");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApplicationForm  $applicationForm
     * @return mixed
     */
    public function delete(User $user, ApplicationForm $applicationForm)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApplicationForm  $applicationForm
     * @return mixed
     */
    public function restore(User $user, ApplicationForm $applicationForm)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ApplicationForm  $applicationForm
     * @return mixed
     */
    public function forceDelete(User $user, ApplicationForm $applicationForm)
    {
        return false;
    }
}

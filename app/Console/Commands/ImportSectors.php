<?php

namespace App\Console\Commands;

use App\Models\Sector;
use Illuminate\Console\Command;

class ImportSectors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:sectors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports sectors from CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $f = fopen(app_path('Data/subsectors.csv'),'r');
        while($line = fgetcsv($f))
        {
            $this->line($line[0]);

            $sectorLine = explode(' - ',$line[2]);

            $sector = Sector::where([
                'eb_code' => $sectorLine[0],
                'name' => $sectorLine[1],
            ])->first();
            if(!$sector)
            {
                $sector = Sector::create([
                    "parent_id" => 0,
                    "eb_code" => $sectorLine[0],
                    "name" => $sectorLine[1],
                ]);
            }

            $subsector = Sector::create([
                "parent_id" => $sector->id,
                "eb_code" => $line[0],
                "name" => $line[1],
            ]);
        }
    }
}

<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Footer extends Component
{
    public $name;

    public $currentStep;

    public $totalSteps;

    public $application;

    public function __construct($name,$currentStep,$totalSteps,$application)
    {
        $this->name = $name;
        $this->currentStep = $currentStep;
        $this->totalSteps = $totalSteps;
        $this->application = $application;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.footer');
    }
}

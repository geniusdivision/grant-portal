<?php

namespace App\Nova\Actions;

use Illuminate\Support\Collection;
use Illuminate\View\View;
use Laravel\Nova\Fields\ActionFields;
use Padocia\NovaPdf\Actions\ExportToPdf;
use App\Models\Application;

class ExportApplicationFormPdf extends ExportToPdf
{
    public $name = "Export PDF";
    /**
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     *
     * @return \Illuminate\View\View
     */
    public function preview(ActionFields $fields, Collection $models) : View
    {
        $model = $models->first();
        return view('applications.pdf-application-form', compact('model'));
    }

     protected function filename(): string
     {
         $application = Application::find(request()->input('resources'));
         return "application_" . $application->applicationForm->slug . "_" .$application->getRouteKey() . ".pdf";
     }

    protected function handleBrowsershotOptions()
    {

        $this->browsershot = $this->browsershot->format("A4");

        if(env('NODE_PATH'))
        {
            $this->browsershot->setNodeBinary(env("NODE_PATH"));
        }

        return $this;
    }

}

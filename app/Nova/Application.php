<?php

namespace App\Nova;

use App\Nova\Actions\RejectApplication;
use App\Nova\Actions\SetStatus;
use App\Nova\Filters\ApplicationStatus;
use App\Nova\Filters\AssignmentStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;
use App\Nova\Actions\ExportApplicationFormPdf;

class Application extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Application::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    //public static $title = 'name';

    public function title()
    {
        return $this->resource->companyName ." - ". $this->resource->applicationForm->name;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

//    public static function indexQuery(NovaRequest $request, $query)
//    {
//        if(Auth::user()->hasRole('Manager'))
//        {
//            return $query->where('status','!=','In progress');
//        }
//        else
//        {
//            //return $query;
//            return $query->where('status','!=','In progress');
//        }
//    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            //ID::make(__('ID'), 'id')->sortable(),
            Text::make('ID',function(){
                return $this->resource->getID();
            }),
            BelongsTo::make('User'),
            Text::make('Company Name',function(){
                return $this->resource->companyName;
            }),
            DateTime::make('Submitted at',function(){
                return $this->resource->submitted_at;
            })->onlyOnDetail(),
            Text::make('Form',function(){
                return $this->resource->applicationForm->name;
            }),

            Text::make('Status',function(){
                return $this->resource->status;
            }),

            Text::make('Project Officer',function(){
                if($this->resource->projectOfficers->count())
                {
                    $officers = [];
                    foreach($this->resource->projectOfficers as $user)
                    {
                        $officers[] = '<a class="text-primary font-bold" href="/nova/resources/users/'.$user->id.'">'.$user->name.'</a>';
                    }
                    return implode(", ",$officers);
                }
                else
                {
                    return "";
                }
                return $this->resource->projectOfficers->count() ? $this->resource->projectOfficers->first()->name : '-';
            })->onlyOnIndex()->asHtml(),

            Panel::make('Project Details',function(){
                return [
                    Text::make('Estimated Start Date',function(){
                        return $this->resource->getField('applicant-details','projectStartDate');
                    })->onlyOnDetail(),
                    Text::make('Estimated End Date',function(){
                        return $this->resource->getField('applicant-details','projectEndDate');
                    })->onlyOnDetail(),
                    Text::make('Project cost',function(){
                        return $this->resource->getField('applicant-details','projectCost');
                    })->onlyOnDetail(),
                    Text::make('Grant requested',function(){
                        return $this->resource->getField('applicant-details','grantRequested');
                    })->onlyOnDetail(),
                    Text::make('Proposed supplier',function(){
                        return $this->resource->getField('applicant-details','proposedSupplier');
                    })->onlyOnDetail(),
                    Text::make('About project',function(){
                        return $this->resource->getField('applicant-details','aboutProject');
                    })->onlyOnDetail(),
                ];
            }),

            BelongsToMany::make('Project Officers','projectOfficers','App\Nova\User'),
        ];
    }

    public static function relatableUsers(NovaRequest $request, $query)
    {
        $application = \App\Models\Application::find($request->resourceId);

        if (!$application) {
            return;
        }
        return $query->whereHas("roles", function($q){ $q->where("name", "Project Officer")->orWhere('name','Manager'); });
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new ApplicationStatus(),
            new AssignmentStatus(),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new SetStatus())->onlyOnDetail(),
            (new RejectApplication())->onlyOnDetail(),
            (new ExportApplicationFormPdf())->onlyOnDetail()
        ];
    }
}

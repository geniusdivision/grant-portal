<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InReviewNotification extends Notification
{
    use Queueable;

    public $application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new GrantMailMessage())
                ->subject('The status of your application has changed to In review')
            ->line('Hi ' . $this->application->user->name .",")
            ->line('Thank you for submitting your ' . $this->application->applicationForm->name . ' application.')
            ->line('The status of your application has changed to <strong>in review</strong>, and will be scored by the grant assessment panel once the current application window closes. The dates for the current application window can be found on the <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/">' . $this->application->applicationForm->name . ' webpage</a>.')
            ->line('Until the application window closes, the ' . $this->application->applicationForm->name . ' team will carry out preliminary checks<sup>*</sup> on applications, and will be in touch if yours requires any additional information.')
            ->line('If you have any further questions, please <a href="mailto:digitalinnovationgrants@barnsley.gov.uk">email</a> our team.')
            ->salutation("Kind regards<br>The ".$this->application->applicationForm->name." team")
            ->smallprint("*The preliminary check relates to section 1 of your application. Due to the competitive nature of the grant, we have not reviewed your answers in section two, as these will be judged by the grant assessment panel.<br><br>");



    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

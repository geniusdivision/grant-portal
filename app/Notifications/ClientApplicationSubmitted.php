<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ClientApplicationSubmitted extends Notification
{
    use Queueable;
    public $application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->application->applicationForm->name ." Application Submitted")
            ->line('Hi ' . $this->application->user->name .",")
            ->line("Thank you for submitting your application for the ".$this->application->applicationForm->name." programme. Please accept this email as confirmation of submission. Your application is now classed as ‘Submitted’. Whilst in this state, you can go back on to the portal and edit it. It will shortly be assigned to our project team who will review the application. When this is happens, the status of the application will change to ‘In Review’. Whilst in this state, you can assess the application in a read only form.")
            ->line("Once reviewed, your application will be moved to one of three status. ‘More Information Required’, ‘Accepted’, or ‘Rejected’. No matter which status it is assigned, you will receive a further email when this happens on what to do next.")
            ->line("At the moment, it can take up to two weeks for the application to be reviewed.")
            ->line('If you have any questions or if you need any assistance, please <a href="mailto:digitalinnovationgrants@barnsley.gov.uk">email</a> and we&rsquo;ll help you.')
            ->salutation("Kind regards<br>The ".$this->application->applicationForm->name." team");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

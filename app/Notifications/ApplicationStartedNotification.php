<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApplicationStartedNotification extends Notification
{
    use Queueable;
    public $application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->application->applicationForm->name ." Application Started")
                    ->line('Hi ' . $this->application->user->name .",")
                    ->line("Thank you for starting an application for the ".$this->application->applicationForm->name." programme. Your application has not been submitted yet, but your progress will be saved.")

                    ->action('Continue Application', route('applications.apply',[$this->application->applicationForm,$this->application]))
                    ->line('If you have any questions or if you need any assistance, please <a href="mailto:digitalinnovationgrants@barnsley.gov.uk">email</a> and we&rsquo;ll help you.')
            ->salutation("Kind regards<br>The ".$this->application->applicationForm->name." team");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

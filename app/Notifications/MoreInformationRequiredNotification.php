<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MoreInformationRequiredNotification extends Notification
{
    use Queueable;
    public $application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new GrantMailMessage)
            ->subject($this->application->applicationForm->name ." Application - More information required")
            ->line('Hi ' . $this->application->user->name .",")
            ->line("Thank you for submitting your ".$this->application->applicationForm->name." application. The ".$this->application->applicationForm->name." team have done a preliminary check<sup>*</sup> of your application, and additional information is required before your application can be scored by the grant assessment panel.")
            ->line("A member of our team has left comments under each question which requires clarification.")
            ->line("Please add any additional information requested to the relevant answer fields. You also can add your own comments to clarify your answers, or to ask the team questions of your own.")
            ->action('View Feedback', route('applications.apply',[$this->application->applicationForm,$this->application]))
            ->line("Once you have revised your application, you will need to re-submit your application by clicking through to the final page, like you did when you first submitted. It’s vital you do this so that we can see your additional answers.")
            ->line('<strong>You will need to resubmit by the application deadline, or your application will be scored in its current state.</strong> The dates for the current application window can be found on the <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/">' . $this->application->applicationForm->name . ' webpage</a>.')
            ->line('If you have any further questions, please <a href="mailto:digitalinnovationgrants@barnsley.gov.uk">email</a> our team.')
            ->salutation("Kind regards<br>The ".$this->application->applicationForm->name." team")
           ->smallprint("*The preliminary check relates to section 1 of your application. Due to the competitive nature of the grant, we have not reviewed your answers in section two, as these will be judged by the grant assessment panel.<br><br>");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApplicationRejectedNotification extends Notification
{
    use Queueable;

    public $application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Application Rejected')
            ->line('Hi ' . $this->application->user->name .",")
            ->line("Thank you for applying for the ".$this->application->applicationForm->name." programme. We’re sorry to inform you that your application has not met the criteria needed be awarded a grant, and your application status has changed to <strong>rejected</strong>.")
            ->line('The '.$this->application->applicationForm->name.' team should have already contacted you by email to provide feedback explaining why the application was unsuccessful, but if you have any other questions please contact us at <a href="mailto:DigitalInnovationGrants@barnsley.gov.uk">DigitalInnovationGrants@barnsley.gov.uk</a>.')
            ->salutation("Kind regards<br>The ".$this->application->applicationForm->name." team");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

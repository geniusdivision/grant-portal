<?php

namespace App\Events;

use App\Models\Pivots\AssignmentsPivot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ApplicationAssigned
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pivot;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AssignmentsPivot $pivot)
    {
        $this->pivot = $pivot;
    }
}

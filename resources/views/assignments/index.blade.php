<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                My Assignments
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="text-xl mb-2 font-medium text-gray-900">In review</h2>
            @if($inReview->count())
            <div class="bg-white shadow overflow-hidden sm:rounded-md">
                <ul class="divide-y divide-gray-200">
                    @foreach($inReview as $application)
                        <li>
                            <a href="{{ route('applications.review',[$application]) }}" class="block hover:bg-gray-50">
                                <div class="px-4 py-4 sm:px-6">
                                    <div class="flex items-center justify-between">
                                        <p class="text-sm font-medium text-red-600 truncate">
                                            {{ $application->applicationForm->name }} (#{{$application->getID() }})
                                        </p>
                                        <div class="ml-2 flex-shrink-0 flex">
                                            <x-status-badge :application="$application"></x-status-badge>
                                        </div>
                                    </div>
                                    <div class="mt-2 sm:flex sm:justify-between items-center">
                                        <div class="sm:flex">
                                            <p class="flex items-center text-sm text-gray-500">
                                                {{ $application->companyName }}
                                            </p>
                                        </div>
                                        <div class="relative pt-1 md:w-2/6">
                                            <p class="text-sm text-gray-500 md:text-right">
                                                <strong>Submitted at:</strong> {{ $application->submitted_at }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @else



                <div class="relative block w-full border-2 border-gray-300 border-dashed rounded-lg p-12 text-center  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    <svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="folder-open" class="mx-auto h-12 w-12 text-gray-400" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M69.08 271.63L0 390.05V112a48 48 0 0 1 48-48h160l64 64h160a48 48 0 0 1 48 48v48H152a96.31 96.31 0 0 0-82.92 47.63z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M152 256h400a24 24 0 0 1 20.73 36.09l-72.46 124.16A64 64 0 0 1 445 448H45a24 24 0 0 1-20.73-36.09l72.45-124.16A64 64 0 0 1 152 256z"></path></g></svg>
                    <span class="mt-2 block text-sm text-gray-900">
    No applications are currently in review.
  </span>
                </div>
            @endif
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-8">
            <h2 class="text-xl mb-2 font-medium text-gray-900">More information required</h2>
            @if($moreInformationRequired->count())
                <div class="bg-white shadow overflow-hidden sm:rounded-md">
                    <ul class="divide-y divide-gray-200">
                        @foreach($moreInformationRequired as $application)
                            <li>
                                <a href="{{ route('applications.review',[$application]) }}" class="block hover:bg-gray-50">
                                    <div class="px-4 py-4 sm:px-6">
                                        <div class="flex items-center justify-between">
                                            <p class="text-sm font-medium text-red-600 truncate">
                                                {{ $application->applicationForm->name }} (#{{$application->getID() }})
                                            </p>
                                            <div class="ml-2 flex-shrink-0 flex">
                                                <x-status-badge :application="$application"></x-status-badge>
                                            </div>
                                        </div>
                                        <div class="mt-2 sm:flex sm:justify-between items-center">
                                            <div class="sm:flex">
                                                <p class="flex items-center text-sm text-gray-500">
                                                    {{ $application->companyName }}
                                                </p>
                                            </div>
                                            <div class="relative pt-1 md:w-2/6">
                                                <p class="text-sm text-gray-500 md:text-right">
                                                    <strong>Submitted at:</strong> {{ $application->submitted_at }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @else

                <div class="relative block w-full border-2 border-gray-300 border-dashed rounded-lg p-12 text-center  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    <svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="folder-open" class="mx-auto h-12 w-12 text-gray-400" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M69.08 271.63L0 390.05V112a48 48 0 0 1 48-48h160l64 64h160a48 48 0 0 1 48 48v48H152a96.31 96.31 0 0 0-82.92 47.63z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M152 256h400a24 24 0 0 1 20.73 36.09l-72.46 124.16A64 64 0 0 1 445 448H45a24 24 0 0 1-20.73-36.09l72.45-124.16A64 64 0 0 1 152 256z"></path></g></svg>
                    <span class="mt-2 block text-sm text-gray-900">
    No applications currently require more information.
  </span>
                </div>
            @endif
        </div>
    </div>
</x-app-layout>

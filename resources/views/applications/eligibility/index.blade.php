<x-app-layout>
    <x-slot name="header">
        <div class="">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ $applicationForm->name }} {{ __('Eligibility Check') }}
            </h2>
            <p class="text-sm  text-gray-500 mt-2">You must demonstrate that your business / project meets all of the eligibility criteria. Any projects that do not meet all of the eligibility criteria should not submit an application.</p>
        </div>
    </x-slot>

    @livewire('eligibility',[$applicationForm])
</x-app-layout>

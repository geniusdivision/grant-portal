<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ $title }}
            </h2>
            @can('create applications')
            <div x-data="{ open: false }" @keydown.escape.stop="open = false; focusButton()" @click.away="open=false" class="relative inline-block text-left">
                <div>
                    <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" id="menu-button" x-ref="button" @click="open=!open">
                        New application
                        <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                        </svg>
                    </button>
                </div>


                <div x-show="open" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" x-ref="menu-items" x-description="Dropdown menu, show/hide based on menu state." role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1" @keydown.arrow-up.prevent="onArrowUp()" @keydown.arrow-down.prevent="onArrowDown()" @keydown.tab="open = false" @keydown.enter.prevent="open = false; focusButton()" @keyup.space.prevent="open = false; focusButton()">
                    <div class="py-1" role="none">
                        @foreach(\App\Models\ApplicationForm::all() as $form)
                        <a href="{{ route('applications.about',$form) }}" class="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">{{ $form->name }}</a>
                        @endforeach
                    </div>
                </div>

            </div>
            @endcan
        </div>
    </x-slot>

    <div class="py-4 sm:py-12">
        <div class="grid lg:grid-cols-4 max-w-7xl mx-auto sm:px-6 lg:px-8 gap-6 lg:gap-8">
            <div class="lg:col-span-3 order-2 lg:order-1">
                @if($applications->count())
                    <div class="bg-white shadow overflow-hidden sm:rounded-md">
                        <ul class="divide-y divide-gray-200">
                            @foreach($applications as $application)
                                <li>
                                    <a href="{{ Auth::user()->hasRole('Project Officer') ? route('applications.review',[$application]) : route('applications.apply',[$application->applicationForm,$application]) }}" class="block hover:bg-gray-50">
                                        <div class="px-4 py-4 sm:px-6">
                                            <div class="flex items-center justify-between">
                                                <p class="text-base font-medium truncate">
                                                    {{ $application->applicationForm->name }}
                                                </p>
                                                <div class="ml-2 flex-shrink-0 flex">
                                                    <x-status-badge :application="$application"></x-status-badge>
                                                </div>
                                            </div>
                                            <span class="text-xs text-gray-500">Ref: <span class="font-bold text-gray-800">{{$application->reference }}</span></span>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @else
                    <div class="bg-white px-4 py-12 bg-white shadow overflow-hidden sm:rounded-md">

                        <div class="text-center">

                            <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-12 w-12 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                            </svg>
                            <h3 class="mt-2 text-sm font-medium text-gray-500">No applications found</h3>
                        </div>

                    </div>
                    @endif


            </div>
            <div class="col-span-1 text-sm text-gray-600 p-4 lg:p-0 order-1 lg:order-2 cms">
                <div class="rounded p-4 bg-white shadow">
                    <h2 class="text-gray-700 mb-4 font-bold text-lg">Need help?</h2>
{{--                    <p class="mb-4">You can find helpful information and advice about completing your application online in our <a href="{{ route('applications.help') }}">help section</a>.</p>--}}
                    <p>If you are struggling to complete your application online, please <a href="mailto:digitalinnovationgrants@barnsley.gov.uk">email us</a> or <a href="https://www.enterprisingbarnsley.co.uk/contact/">contact us</a></p>
                </div>

            </div>
        </div>
    </div>

</x-app-layout>

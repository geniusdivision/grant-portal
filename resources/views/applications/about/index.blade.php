<x-app-layout>
    <x-slot name="header">
        <div class="">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ $applicationForm->name }}
            </h2>

        </div>
    </x-slot>

    <div class="py-4 sm:py-12">
        <div class="grid lg:grid-cols-4 max-w-7xl mx-auto sm:px-6 lg:px-8 gap-6 lg:gap-8">
            <div class="lg:col-span-3 order-2 lg:order-1">
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                        <div class="cms">
                            {!! $applicationForm->description !!}
                        </div>
                    </div>
                </div>
                <div class="flex justify-end mt-4 pr-2 sm:pr-0">
                    <div>
                        @if($applicationForm->guidance_document)
                            <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-grant-guidance/" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 mr-4" target="_blank">
                                Download guidance document
                            </a>
                            @endif
                    </div>

                    <a href="{{ route('applications.eligibility',$applicationForm) }}" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                        Continue to application
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

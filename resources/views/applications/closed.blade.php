<x-app-layout>
    <x-slot name="header">
        <div class="">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ $applicationForm->name }}
            </h2>

        </div>
    </x-slot>

    <div class="py-4 sm:py-12">
        <div class="grid lg:grid-cols-4 max-w-7xl mx-auto sm:px-6 lg:px-8 gap-6 lg:gap-8">
            <div class="lg:col-span-3 order-2 lg:order-1">
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                        <div class="cms">
                            {!! $applicationForm->closed_message !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

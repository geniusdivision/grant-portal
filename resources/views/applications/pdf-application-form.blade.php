<!doctype html>
<html>
<head>
    <title>Application Form Pdf Template</title>
    <meta charset="UTF-8" />
    @foreach ($stylesContents as $stylesContent)
        <style>
            {{ $stylesContent }}
        </style>
    @endforeach
    <style>
        html, body{
            height: 100%;
            -webkit-print-color-adjust: exact;
            font-family: Arial, sans-serif;
        }

        h1 {
            font-size: 22px;
            font-weight: bold;
            margin-bottom: 15px;
            text-decoration: underline;
            text-align: center;
        }

        h2 {
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 15px;
            text-decoration: underline;
        }

        h4 {
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 10px;
        }

        h5 {
            font-weight: bold;
        }

        p {
            margin-bottom: 15px;
        }

        .underline {
            text-decoration: underline;
        }

        a {
            color: blue;
        }

        table td, table th {
            padding: 5px 15px;
        }

        table p {
            margin-bottom: 0;
        }

        ul {
            list-style: disc;
            margin-left: 15px;
            padding-left: 15px;
            margin-bottom: 5px;
        }

        @media print {
            .page-break {
                page-break-after: always;
            }
        }
    </style>
</head>
<body class="tailwind-container">




<section class="h-full px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left mt-9">
        <h1>DIGITAL INNOVATION GRANT – APPLICATION FORM</h1>
        <p class="text-sm">The Digital Innovation Grant Programme is an ERDF funded project and Barnsley MBC are the
            accountable body. The project is receiving up to £1.63m of funding from the England European
            Regional Development Fund as part of the European Structural and Investment Funds Growth
            Programme 2014-2020. The Ministry of Housing, Communities and Local Government (and in
            London the intermediate body Greater London Authority) is the Managing Authority for European
            Regional Development Fund. Established by the European Union, the European Regional
            Development Fund helps local areas stimulate their economic development by investing in
            projects which will support innovation, businesses, create jobs and local community
            regenerations. For more information visit <a href="https://www.gov.uk/european-growth-funding">https://www.gov.uk/european-growth-funding</a></p>
    </div>

    <div class="w-full text-left">
        <h2>Eligibility Criteria</h2>
        <p class="text-sm">You must demonstrate that your business / project meets <span class="underline">all</span> of the eligibility criteria. Any
            projects that do not meet <span class="underline">all</span> of the eligibility criteria will <span class="underline">not</span> progress to stage 2 – Application</p>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-28">
            <thead>
            <tr>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm" colspan="2">Eligibility Criteria</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm">Y/N</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="border border-gray-500 text-center" align="top">1</td>
                <td class="border border-gray-500 text-xs">
                    <p>Does your business have its registered and/or trading office address in any of the following local authorities? ‘Virtual’ tenancy agreements do not qualify.</p>
                    <ul>
                        <li>Barnsley</li>
                        <li>Doncaster</li>
                        <li>Rotherham</li>
                        <li>Sheffield</li>
                    </ul>
                    <p>Barnsley only: will you participate in Barnsley Council’s <a href="https://www.barnsley.gov.uk/services/business-information/more-and-better-jobs-employer-promise/">Employer Promise initiative</a>?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>

            <tr>
                <td class="border border-gray-500 text-center">2</td>
                <td class="border border-gray-500 text-xs">
                    <p>Is your business categorised as a Small to Medium Sized Enterprise (SME)?</p>
                    <ul>
                        <li>Fewer than 250 full-time equivalent (FTE) employees</li>
                        <li>Less than £43m turnover</li>
                    </ul>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>

            <tr>
                <td class="border border-gray-500 text-center">3</td>
                <td class="border border-gray-500 text-xs">
                    <p>Has your business been established for at least 12 months? If not, will you complete additional verification checks to examine the credentials and financial viability of your business?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>

            <tr>
                <td class="border border-gray-500 text-center">4</td>
                <td class="border border-gray-500 text-xs">
                    <p class="mb-2">Does your business generate the majority of its turnover from trading with other businesses, or, will your application demonstrate how the grant supported project will lead to this?</p>
                    <p>OR: If your business generates the majority of its turnover from trading with consumers (eg. retail), can you confirm you serve customers outside of the 'local area' (eg. online retail), and/or that your project will support this?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>

            <tr>
                <td class="border border-gray-500 text-center">5</td>
                <td class="border border-gray-500 text-xs">
                    <p>Does your business have private finance available, either your own, from a bank or other commercial finance to invest alongside the grant?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>

            <tr>
                <td class="border border-gray-500 text-center">6</td>
                <td class="border border-gray-500 text-xs">
                    <p>Can you demonstrate financial viability of the project and the need for grant support?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>
            </tbody>
        </table>
    </div>
</section>
<section class="h-full px-12" style="page-break-before: always;">
    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>
    <div class="w-full text-left mt-12">
        <table class="auto border-collapse border border-gray-500 mb-28">
            <thead>
            <tr>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm" colspan="2">Eligibility Criteria</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm">Y/N</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="border border-gray-500 text-center">7</td>
                <td class="border border-gray-500 text-xs">
                    <p>Can you confirm that your business / project will not be engaged in the following industries / activities?</p>
                    <ul>
                        <li>Retailing or providing localised business to consumer services.</li>
                        <li>Day Nurseries</li>
                        <li>Fishery & Aquaculture</li>
                        <li>Primary Production</li>
                        <li>Synthetic fibres / textiles & clothing</li>
                        <li>Shipbuilding</li>
                        <li>Coal & Steel</li>
                        <li>Banking and Insurance</li>
                        <li>Generalised school age education</li>
                        <li>Processing and marketing of agricultural products</li>
                    </ul>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>

            <tr>
                <td class="border border-gray-500 text-center">8</td>
                <td class="border border-gray-500 text-xs">
                    <p>Has your businesses received less than 200,000 euros of state aid over 3 consecutive fiscal years?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>
            <tr>
                <td class="border border-gray-500 text-center">9</td>
                <td class="border border-gray-500 text-xs">
                    <p>Can you confirm that your business has not already received grant funding for this project, either from Enterprising Barnsley (e.g. Business Productivty, Restart & Recovery, Kickstarting Tourism, and other grants) or other bodies?</p>
                </td>
                <td class="border border-gray-500 text-center">Y</td>
            </tr>
            </tbody>
        </table>
    </div>
</section>
<section class="px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left border-t-2 border-black mt-9 pt-2">
        <h2>Section 1 - Applicant Details</h2>
        <h4>Applicant Business Details</h4>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border-gray-500 w-full mb-1 flex flex-col">
            <tbody>
            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Business Name</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->companyName }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Company Position</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','companyPosition') }}</td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Business Address Line 1</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','address1') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Business Address Line 2</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','address2') }}</td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Town/City</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','city') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Postcode</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','postcode') }}</td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Telephone</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','telephone') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Mobile</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','mobile') }}</td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Email Address</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','email') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Website</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','website') }}</td>
            </tr>
            <?php $businessType = $model->getField('applicant-details','businessType'); ?>
            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Business Size</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','businessSize') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Business Type</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $businessType }}</td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Trading Since</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','tradingStartDate') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Social Enterprise</td>
                <td class="border border-gray-500 text-xs w-1/4">
                    @if($model->getField('applicant-details','socialEnterprise') == 1)
                        Y
                    @else
                        N
                    @endif
                </td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">
                    @if($businessType == "Limited Company")
                        Limited Company Registration Number
                    @elseif($businessType == "Charity")
                        Charity Registration Number
                    @elseif($businessType == "Partnership")
                        Partnership Registration Number
                    @else
                        Sole Trader UTR
                    @endif
                </td>
                <td class="border border-gray-500 text-xs w-1/4">
                    @if($businessType != "Self Employed")
                        {{ $model->getField('applicant-details','companyNumber') }}
                    @else
                        {{ $model->getField('applicant-details','soleTraderUTR') }}
                    @endif
                </td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Business Sector</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ \App\Models\Sector::find($model->getField('applicant-details','businessSector')) ? \App\Models\Sector::find($model->getField('applicant-details','businessSector'))->name : ""  }}</td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Registered In</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','registeredLocation') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">VAT Number</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','vatNumber') }}</td>
            </tr>
            </tbody>
        </table>
        <table class="auto border-collapse  border-gray-500 mb-5 w-full flex flex-col">
            <tbody>
            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-xs w-2/6">Number of Local FTEs</td>
                <td class="border border-gray-500 text-xs w-2/6">{{ $model->getField('applicant-details','localEmployees') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-2/6">Number of National FTEs</td>
                <td class="border border-gray-500 text-xs w-2/6">{{ $model->getField('applicant-details','nationalEmployees') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-2/6">Number of Global FTEs</td>
                <td class="border border-gray-500 text-xs w-2/6">{{ $model->getField('applicant-details','globalEmployees') }}</td>
            </tr>
            </tbody>
        </table>
    </div>

    @if($businessType != "Sole Trader")

        <div class="w-full text-left">
            <h4>{{ $businessType == "Limited Company" ? "Company Directors" : "Senior Leadership Team" }}</h4>
            @if($businessType == "Limited Company")
                <p class="text-sm">We will need to know the names and title of each of your directors.</p>
            @elseif($businessType == "Charity")
                <p class="text-sm">We will need to know the name of your trustees and anyone on your board.</p>
            @elseif($businessType == "Partnership")
                <p class="text-sm">We will need to know the names and of everyone in the partnership.</p>
            @endif

        </div>

        <div class="w-full text-left">
            <table class="auto border-collapse border-gray-500 mb-5 w-full" style="">
                <thead>
                <tr class="flex">
                    <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/2">Name</th>
                    <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/2">Position</th>
                </tr>
                </thead>
                <tbody>
                @foreach($model->getField('applicant-details', 'directors') as $director)
                    <tr class="flex">
                        <td class="border border-gray-500 text-xs w-1/2">{{ $director['name'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/2">{{ $director['position'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    @endif



</section>

{{-- Break page so looped info doesnt go over two pages --}}
<div class="page-break"></div>

<section class="px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left mt-10">
        <h4>Statement of Previous Aid</h4>
        <p class="text-sm">The Digital Innovation Grant project is part funded by the European Regional Development Fund as part of the European Structural and Investment Funds Growth Programme 2014-2020. Financial and non-financial support given to your company as part of this project is categorised as ‘De Minimis Aid, Commission Regulation (EU) No 1407/2013’. This allows a single undertaking to receive up to €200,000 of De Minimis Aid over a rolling three-year period comprising the current and previous two financial years. If your business is part of a group this aid ceiling may be cumulative; please discuss with your Advisor if you believe this applies to you.</p>

        <p class="text-sm underline">It is your responsibility to check whether funds received are ‘De Minimis’. If in doubt, please check with the funding sources</p>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border-gray-500 mb-10 w-full">
            <thead>
            <tr class="flex">
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">Date funding granted</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">Funding body</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">Amount (£)</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">Purpose</th>
            </tr>
            </thead>
            <tbody>
            @if($previousAidLines = $model->getField('applicant-details', 'previousAid'))
                @foreach($previousAidLines as $previousAid)
                    <tr class="flex">
                        <td class="border border-gray-500 text-xs w-1/4">{{ $previousAid['date'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/4">{{ $previousAid['body'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/4">{{ $previousAid['amount'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/4">{{ $previousAid['purpose'] }}</td>
                    </tr>
                @endforeach
            @else
                <tr class="flex">
                    <td class="border border-gray-500 text-xs w-1/4"></td>
                    <td class="border border-gray-500 text-xs w-1/4"></td>
                    <td class="border border-gray-500 text-xs w-1/4"></td>
                    <td class="border border-gray-500 text-xs w-1/4"></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>


    <div class="w-full text-left">
        <h4>Project Details</h4>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-1 w-full">
            <tbody>
            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Estimated Start Date</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','projectStartDate') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Estimated End Date</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','projectEndDate') }}</td>
            </tr>

            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Total Project Cost (£)</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','projectCost') }}</td>
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Total Grant Requested (£)</td>
                <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('applicant-details','grantRequested') }}</td>
            </tr>
            </tbody>
        </table>

        <table class="auto border-collapse border border-gray-500 mb-10 w-full">
            <tbody>
            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Proposed Supplier</td>
                <td class="border border-gray-500 text-xs w-3/4">{{ $model->getField('applicant-details','proposedSupplier') }}</td>
            </tr>
            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">About Your Project</td>
                <td class="border border-gray-500 text-xs w-3/4">{{ $model->getField('applicant-details','aboutProject') }}</td>
            </tr>

            </tbody>
        </table>
    </div>

</section>

{{-- Break page for new section --}}
<div class="page-break"></div>

<section class="px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left border-t-2 border-black mt-9 pt-2">
        <h2>Section 2 - Assessment</h2>
        <h4>What is the project?</h4>
        <ul class="text-sm">
            <li>What is the business looking to acquire with the grant funding?</li>
            <li>What is the overall objective of the project? - Explain as expressively as possible the ultimate, "big picture" vision and purpose of your completed endeavor.</li>
            <li>Explain why grant funding is needed, with reference to the viability of the project with or without grant funding.</li>
        </ul>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-10 w-full mt-1">
            <tbody>
            <tr>
                <td class="border border-gray-500 text-xs">{{ $model->getField('assessment','businessProblem') }}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="w-full text-left">
        <h4>How will this project help the growth of your business?</h4>
        <ul class="text-sm">
            <li>Describe in detail the problem(s) currently being faced by the business.</li>
            <li>Describe how the investment will resolve the problems and contribute to the development or creation of new products or services</li>
            <li>Evidence how the investment is aligned to the companies overarching growth strategy, referencing your ‘target market’ i.e. local, regional, national, international).</li>
        </ul>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-10 w-full mt-1">
            <tbody>
            <tr>
                <td class="border border-gray-500 text-xs">{{ $model->getField('assessment','digitalTransformation') }}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="w-full text-left">
        <h4>Who Will Benefit from your Project?</h4>
        <p class="text-sm">Summarise the economic benefits outputs that the project will be projected to deliver. This could include:</p>
        <ul class="text-sm">
            <li>Introducing new products, services or procedures</li>
            <li>Accessing new markets</li>
            <li>Safeguarding existing jobs</li>
            <li>Creating New Job Opportunities</li>
            <li>Increased Turnover</li>
            <li>Increased Profit</li>
        </ul>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-10 w-full mt-1">
            <tbody>
            <tr>
                <td class="border border-gray-500 text-xs">{{ $model->getField('assessment','economicImpact') }}</td>
            </tr>
            </tbody>
        </table>
    </div>

</section>

{{-- Break page for new section --}}
<div class="page-break"></div>

<section class="px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left mt-10">
        <table class="auto border-collapse border-gray-500 mb-10 w-full">
            <thead>
            <tr class="flex">
                <th class="border border-gray-500 text-center bg-gray-300 text-sm" colspan="4">Economic Impact – Please provide details of any expected economic impacts if your grant application is successful.</th>
            </tr>
            <tr class="flex">
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">Impact Type</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">£/Numbers</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">% Increase</th>
                <th class="border border-gray-500 text-center bg-gray-300 text-sm w-1/4">Timescale</th>
            </tr>
            </thead>
            <tbody>
            @if($economicImpact = $model->getField('assessment','economicImpactValues'))
                @foreach($economicImpact as $impact)
                    <tr class="flex">
                        <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">{{ $impact['impact'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/4">{{ $impact['value'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/4">{{ $impact['increase'] }}</td>
                        <td class="border border-gray-500 text-xs w-1/4">{{ $impact['timescale'] }}</td>
                    </tr>
                @endforeach
            @else
                <tr class="flex">
                    <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Jobs Created (FTEs)</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','jobsCreatedValue') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','jobsCreatedIncrease') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','jobsCreatedTimescale') }}</td>
                </tr>
                <tr class="flex">
                    <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Jobs Safeguarded</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','jobsSafeguardedValue') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','jobsSafeguardedIncrease') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','jobsSafeguardedTimescale') }}</td>
                </tr>
                <tr class="flex">
                    <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Increased Turnover</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','increasedTurnoverValue') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','increasedTurnoverIncrease') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','increasedTurnoverTimescale') }}</td>
                </tr>
                <tr class="flex">
                    <td class="border border-gray-500 bg-gray-300 text-xs w-1/4">Increased Profit</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','increasedProfitValue') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','increasedProfitIncrease') }}</td>
                    <td class="border border-gray-500 text-xs w-1/4">{{ $model->getField('assessment','increasedProfitTimescale') }}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>



    <div class="w-full text-left">
        <h4>Can you explain the rationale behind your chosen supplier/provider, demonstrating the chosen supplier’s experience and/or value for money?</h4>
        <p class="text-sm">Summarise the economic benefits outputs that the project will be projected to deliver. This could include:</p>
        <ul class="text-sm">
            <li>Satisfy that the proposals are viable, offer value for money, are realistic and deliverable.</li>
            <li>Highlight which supplier(s) you have approached and why.</li>
            <li>Detail the quotes received and the reason why a specific supplier was chosen. If you have an existing relationship with the supplier, please state what this is.</li>
            <li>If only one supplier has been approached explain why.</li>
        </ul>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-10 w-full mt-1">
            <tbody>
            <tr>
                <td class="border border-gray-500 text-xs">{{ $model->getField('assessment','chosenSupplier') }}</td>
            </tr>
            </tbody>
        </table>
    </div>

</section>

{{-- Break page for new section --}}
<div class="page-break"></div>

<section class="px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left border-t-2 border-black mt-9 pt-2">
        <h2>Section 3 - Confirmations</h2>
    </div>


    <div class="w-full text-left">
        <table class="auto border-collapse border border-gray-500 mb-28">
            <tbody>
            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Privacy Policy</h5>
                    <p>I/We have read and accept the <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-privacy-policy/">Privacy Policy</a> and consent to usage of the information provided for the purposes described.</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
                </td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Data Usage Statement</h5>
                    <p class="mb-2">The Digital Innovation Grant project will use your personal details and information to set up and administer your relationship with us, to provide you with advice and information on the services we offer which we believe will be of interest to you.  We will also use your information for record keeping, statistical and evaluation purposes.  By completing this form you confirm that personal details you have provided are correct and you give us permission for this information to be stored and processed as detailed above and in line with our Privacy Policy.  You have a right to request a copy of the personal information we hold about you and you also have the right to correct any inaccuracies in the information we hold about you.  Further information about how we deal with your personal information can be found in our Privacy Policy.</p>
                    <p>I/We have read and accept the Data Usage Statement and consent to usage of the information provided for the purposes described.</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Authorisation</h5>
                    <p>I/We confirm that I/We are authorised to submit this application on behalf of the business and confirm that I/We understand the requirements of De Minimis and acknowledge that if the business fails to meet the eligibility requirements it may become liable to repay the full value of assistance provided</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
                </td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Terms and Conditions</h5>
                    <p>I/We agree to comply with the <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-terms-conditions/">Terms and Conditions</a> of the application and any associated guidance documents.</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
                </td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Future Support</h5>
                    <p>I/We understand that completion of this form neither entitles nor requires the business to take part in any particular business support services in the future.</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
                </td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Document Retention</h5>
                    <p>I/We acknowledge the document retention requirement as detailed in the Application Guidance and confirm I/We will retain the original document until further notice.</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
                </td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm">
                    <h5>Accuracy of Details</h5>
                    <p>The details provided on this form are current and accurate to the best of my/our knowledge.</p>
                </td>
                <td class="border border-gray-500 text-center">
                    Y
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</section>

{{-- Break page for new section --}}
<div class="page-break"></div>

<section class="px-12">

    <div class="flex w-full pt-12 justify-between items-center">
        <div>
            <img src="{{ public_path().'/img//DIG-Logo.png'}}" class="" width="180">
        </div>
        <div>
            <img src="{{ public_path().'/img/ERDF-Logo.png'}}" cless="" width="230">
        </div>
    </div>

    <div class="w-full text-left mt-9 pt-2">
        <h2>Final Signature</h2>
    </div>

    <div class="w-full text-left">
        <table class="auto border-collapse border-gray-500 w-full mb-1 flex flex-col">
            <tbody>
            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm w-5/6">I/We confirm that I/We are authorised to sign on behalf of the business and confirm that I/We acknowledge that if the business fails to meet the eligibility requirements it may become liable to repay the full value of assistance provided</td>
                <td class="border border-gray-500 text-xs w-1/6"></td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm w-5/6">I / We can confirm the company is a small medium-sized enterprise (SME). I / we employ fewer than 250 persons and have an annual turnover not exceeding 50 million Euros, and/or an annual balance sheet total not exceeding 43 million Euros</td>
                <td class="border border-gray-500 text-xs w-1/6"></td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm w-5/6">I / We acknowledge the document retention requirement as detailed in the Application Guidance and confirm I / We will retain the original documentation about the grant until at least December 2033</td>
                <td class="border border-gray-500 text-xs w-1/6"></td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm w-5/6">I / We agree to comply with the Terms and Conditions provided with the application form</td>
                <td class="border border-gray-500 text-xs w-1/6"></td>
            </tr>

            <tr>
                <td class="border border-gray-500 bg-gray-300 text-sm w-5/6">The details provided on this form are current and accurate to the best of my knowledge.</td>
                <td class="border border-gray-500 text-xs w-1/6"></td>
            </tr>
            </tbody>
        </table>
        <table class="auto border-collapse  border-gray-500 mb-5 w-full flex flex-col">
            <tbody>
            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-sm w-1/6">Signed (client)</td>
                <td class="border border-gray-500 text-sm w-2/6"></td>
                <td class="border border-gray-500 bg-gray-300 text-sm w-1/6">Date</td>
                <td class="border border-gray-500 text-sm w-2/6"></td>
            </tr>
            <tr class="flex">
                <td class="border border-gray-500 bg-gray-300 text-sm w-1/6">Print Name</td>
                <td class="border border-gray-500 text-sm w-2/6"></td>
                <td class="border border-gray-500 bg-gray-300 text-sm w-1/6">Position</td>
                <td class="border border-gray-500 text-sm w-2/6"></td>
            </tr>
            </tbody>
        </table>
    </div>



    <div class="w-full text-left mt-4 py-0.5 border-t border-gray-300">
        <p class="mb-0 text-xs"><b>Application</b> - {{ $model->getRouteKey() }}</p>
    </div>
    <div class="w-full text-left py-0.5 border-t border-b border-gray-300">
        <p class="mb-0 text-xs"><b>Submitted by</b> - {{ $model->user->name }} ({{ $model->user->email }})</p>
    </div>
    <div class="w-full text-left py-0.5">
        <p class="text-xs"><b>DIG-C-02 Version 1.</p>
    </div>


</section>
</body>
</html>

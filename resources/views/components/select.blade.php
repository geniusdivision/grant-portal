@props(['disabled' => false,'options' => null])

<select {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'mt-1 block w-full border border-gray-300 bg-white rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 sm:text-sm']) !!}>
    @if($options !== null && count($options))
        @foreach($options as $key => $option)
            <option value="{{ $key }}">{{ $option }}</option>
        @endforeach
    @else
        {{ $slot }}
    @endif
</select>

@props(['disabled' => false, 'addon' => 'X'])

@php

$divClass = 'mt-1 max-w-lg flex rounded-md shadow-sm';
$addonClass = 'inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm';
$inputClass = 'flex-1 block w-full focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 min-w-0 rounded-none rounded-r-md border-gray-300';

@endphp

<div class="flex">
    <div class="{{ $divClass }}">
        <span class="{{ $addonClass }}">{{ $addon }}</span>
        <input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => $inputClass]) !!} >
    </div>
</div>

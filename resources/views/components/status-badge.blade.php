<p class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-{{ \App\Models\Application::color($application->status) }}-100 text-{{ \App\Models\Application::color($application->status) }}-800">
    {{ $application->status }}
</p>

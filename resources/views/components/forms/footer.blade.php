<nav class="border-t border-gray-200 px-4 mt-8 flex items-center justify-between sm:px-0 pt-2">
    <div class="-mt-px w-0 flex-1 flex">
        @if($currentStep > 1)
            <a href="#" wire:click="onPrev" class="<?= $currentStep == $totalSteps ? "" : "pt-2" ?> pr-1 inline-flex items-center text-sm font-medium text-gray-500 hover:text-gray-700">
                <!-- Heroicon name: solid/arrow-narrow-left -->
                <svg class="mr-3 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M7.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l2.293 2.293a1 1 0 010 1.414z" clip-rule="evenodd" />
                </svg>
                Previous
            </a>
        @endif
    </div>

    <div class="-mt-px w-0 flex-1 flex justify-end">
        @if($currentStep != $totalSteps)
        <a href="#" wire:click.prevent="onNext" class="pt-2 pl-1 inline-flex items-center text-sm font-medium text-gray-500 hover:text-gray-700">
            @if($application->status == "In progress" || $application->status == "More information required")
                Save and continue
            @else
                Next
            @endif
            <!-- Heroicon name: solid/arrow-narrow-right -->
            <svg class="ml-3 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd" />
            </svg>
        </a>
        @else
            @can('review applications')
                @if($application->status == "In review")
                    <button wire:click.prevent="onMoreInformationRequired" onclick="return confirm('Are you sure?')" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-yellow-600 hover:bg-yellow-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-yellow-500">
                        More information required
                    </button>
                    <button wire:click.prevent="onReject" onclick="return confirm('Are you sure you wish to reject this application?')" class="md:ml-2 inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" onclick="confirm('Are you sure you want to reject this application? This cannot be undone.')">
                        Reject
                    </button>
                    <button wire:click.prevent="onAccept" onclick="return confirm('Are you sure you wish to accept this application?')" class="md:ml-2 inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500" onclick="confirm('Are you sure you want to reject this application? This cannot be undone.')">
                        Accept
                    </button>
                @endif
            @endcan
            @can('create applications')
                @if($application->status == "In progress")
                    <button {{ $application->applicationForm->applicationWindowClosed() ? "disabled" : "" }} onclick="return confirm('Are you sure?')" wire:click.prevent="onSubmit" wire:loading.attr="disabled" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 disabled:opacity-25">
                        Submit application
                    </button>
                    @elseif($application->status == "More information required")
                        <button onclick="return confirm('Are you sure?')" wire:click.prevent="onResubmit" wire:loading.attr="disabled" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                            Resubmit application
                        </button>

                @endif
            @endcan
        @endif
    </div>
</nav>

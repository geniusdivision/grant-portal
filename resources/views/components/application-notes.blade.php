<div class="px-4 md:px-0">
    <ul class="divide-y divide-gray-200">
        <li class="py-4">
            <div class="flex space-x-3">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixqx=BdTcjmUJeW&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80" alt="">
                <div class="flex-1 space-y-1">
                    <div class="flex items-center justify-between">
                        <h3 class="text-sm font-medium">
                            You
                        </h3>
                        <p class="text-sm text-gray-500">
                            1h
                        </p>
                    </div>
                    <p class="text-sm text-gray-500">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eleifend, magna tempor tristique malesuada, ante turpis lacinia est, sit amet ultricies sem metus quis felis. Aenean ut ornare ante.
                    </p>
                </div>
            </div>
        </li>

        <li class="py-4">
            <div class="flex space-x-3">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixqx=BdTcjmUJeW&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80" alt="">
                <div class="flex-1 space-y-1">
                    <div class="flex items-center justify-between">
                        <h3 class="text-sm">
                            <span class="font-medium">Ben Hawley</span>
                        </h3>
                        <p class="text-sm text-gray-500">
                            1h
                        </p>
                    </div>
                    <p class="text-sm text-gray-500">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eleifend, magna tempor tristique malesuada, ante turpis lacinia est, sit amet ultricies sem metus quis felis. Aenean ut ornare ante.
                    </p>
                </div>
            </div>
        </li>
    </ul>

    <div class="md:pl-14 border-t border-gray-200">

        <p class="text-sm uppercase font-bold my-3 text-gray-400 tracking-wide">
            Reply
        </p>

        <form action="">
            <x-textarea class="w-full">
            </x-textarea>

            <div class="flex justify-end mt-2">
                <button type="submit" class="text-sm border-2 border-green-300 rounded-full text-green-500 px-3 py-1 font-bold uppercase bg-white hover:bg-green-100 hover:text-green-700">
                    Add Note
                </button>
            </div>

        </form>

    </div>
</div>

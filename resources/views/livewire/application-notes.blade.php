<div class="px-4 md:px-0 mt-8">
    @if($this->showNotes)
    <ul class="divide-y divide-gray-200">
        @foreach($notes as $note)
            <li class="py-4">
                <div class="flex space-x-3">
                    <img class="h-12 w-12 rounded-full" src="{{ $note->user->profile_photo_url }}" alt="">
                    <div class="flex-1 space-y-1">
                        <div class="flex items-center justify-between">
                            <h3 class="text-sm font-medium">
                                {{ $note->user->id == Auth::user()->id ? "You" : $note->user->name }}
                            </h3>
                            <p class="text-sm text-gray-500">
                                {{ $note->created_at->diffForHumans() }}
                            </p>
                        </div>
                        <p class="text-sm text-gray-500">
                            {{ $note->note }}
                        </p>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>

    <div class="md:pl-14 border-t border-gray-200">
        <div class="mt-4">
            <x-jet-label for="total_grant_requested" value="{{ __('Note') }}" class="sr-only" />

            <x-textarea class="w-full text-sm" wire:model="noteText" placeholder="Add a note"></x-textarea>
            <x-jet-input-error for="noteText" class="mt-2" />


            <div class="flex justify-end mt-2">
                <button type="submit" wire:click="onAddNote" class="text-sm border-2 border-green-300 rounded-full text-green-500 px-3 py-1 font-bold uppercase bg-white hover:bg-green-100 hover:text-green-700">
                    Add Note
                </button>
            </div>

        </div>

    </div>
    @endif
</div>

<div>
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">

            <div class="sm:flex justify-between items-center">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Review Application #{{ $application->id }}
                </h2>
            </div>
        </div>
    </header>
</div>

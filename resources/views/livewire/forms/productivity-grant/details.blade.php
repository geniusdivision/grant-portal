<div>
    <x-forms.header :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div>
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Applicants details</h3>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <form action="#" method="POST">
                            <div class="shadow sm:rounded-md sm:overflow-hidden">
                                <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-2">
                                            <x-jet-label for="business_name" value="{{ __('Business name') }}" />
                                            <x-jet-input id="business_name" type="text" class="mt-1 block w-full" autocomplete="business_name" />
                                            <x-jet-input-error for="business_name" class="mt-2" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-2">
                                            <x-jet-label for="applicant_name" value="{{ __('Applicant name') }}" />
                                            <x-jet-input id="applicant_name" type="text" class="mt-1 block w-full" autocomplete="applicant_name" />
                                            <x-jet-input-error for="applicant_name" class="mt-2" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-2">
                                            <x-jet-label for="parent_company" value="{{ __('Parent company') }}" />
                                            <x-jet-input id="parent_company" type="text" class="mt-1 block w-full" autocomplete="parent_company" />
                                            <x-jet-input-error for="parent_company" class="mt-2" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-2">
                                            <x-jet-label for="project_location" value="{{ __('Project location') }}" />
                                            <x-jet-input id="project_location" type="text" class="mt-1 block w-full" autocomplete="project_location" />
                                            <x-jet-input-error for="project_location" class="mt-2" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Project details</h3>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="estimated_start_date" value="{{ __('Estimated start date') }}" />
                                        <x-jet-input id="estimated_start_date" type="date" class="mt-1 block w-full" autocomplete="estimated_start_date" placeholder="mm-yyyy" />
                                        <x-jet-input-error for="estimated_start_date" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="estimated_end_date" value="{{ __('Estimated start date') }}" />
                                        <x-jet-input id="estimated_end_date" type="date" class="mt-1 block w-full" autocomplete="estimated_end_date" placeholder="mm-yyyy" />
                                        <x-jet-input-error for="estimated_end_date" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="total_project_cost" value="{{ __('Total project cost') }}" />
                                        <x-input-group id="total_project_cost" type="text" addon="£"></x-input-group>
                                        <x-jet-input-error for="total_project_cost" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="total_grant_requested" value="{{ __('Total grant requested') }}" />
                                        <x-input-group id="total_grant_requested" type="text" addon="£"></x-input-group>
                                        <x-jet-input-error for="total_grant_requested" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="proposed_supplier" value="{{ __('Proposed supplier') }}" />
                                        <x-jet-input id="proposed_supplier" type="text" class="mt-1 block w-full" autocomplete="proposed_supplier" />
                                        <x-jet-input-error for="proposed_supplier" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="about_project" value="{{ __('About your project') }}" />
                                        <x-textarea id="about_project" type="text" class="mt-1 block w-full" autocomplete="about_project" rows="12" />
                                        <x-jet-input-error for="about_project" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

        </div>
    </div>
</div>

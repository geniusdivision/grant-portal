<div>
    <x-forms.header :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">How do you currently measure productivity in your business?</h3>
                            <p class="mt-1 max-w-2xl text-sm text-gray-500 font-bold">Available Marks: 10</p>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="about_project" value="Provide details of current approaches you take to measuring productivity in as simplistic way as possible. The measurements must relate to the particular process(es) which are relevant to this application and the improvements your business is trying to create. Where possible, please provide details of reporting frequency, methodology and rationale." class="mb-4" />
                                        <x-textarea id="about_project" type="text" class="mt-1 block w-full" autocomplete="about_project" rows="12" />
                                        <x-jet-input-error for="about_project" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section class="mt-4">
                            <div class="bg-white shadow sm:rounded-lg sm:overflow-hidden">
                                <div class="divide-y divide-gray-200">

                                    <div class="px-4 py-6 sm:px-6">
                                        <ul class="space-y-8">
                                            <li>
                                                <div class="flex space-x-3">
                                                    <div class="flex-shrink-0">
                                                        <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&amp;ixqx=BdTcjmUJeW&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80" alt="">
                                                    </div>
                                                    <div>
                                                        <div class="text-sm">
                                                            <a href="#" class="font-medium text-gray-900">Leslie Alexander</a>
                                                        </div>
                                                        <div class="mt-1 text-sm text-gray-700">
                                                            <p>Could you add more information about how you measure productivity in your business?</p>
                                                        </div>
                                                        <div class="mt-2 text-sm space-x-2">
                                                            <span class="text-gray-500 font-medium">4d ago</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

            </div>


            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />
        </div>
    </div>
</div>

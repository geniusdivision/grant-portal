<div>
    <x-forms.header :application="$application" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div>
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900 mb-2">Productivity impact</h3>
                            <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lorem nisi, commodo posuere tincidunt nec, pulvinar quis arcu. Phasellus porttitor rutrum mattis.</p>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2 p-4 sm:p-0">
                        @foreach($productivityImpact as $impact)
                        <div class="rounded-lg bg-white overflow-hidden shadow divide-y divide-gray-200 md:grid-cols-3 md:divide-y-0 md:divide-x mb-4">
                            <div class="px-4 py-5 sm:p-6">
                                <div class="flex justify-between">
                                    <div class="text-base font-normal text-gray-900">
                                        {{ $impact['impact'] }} <span class="text-sm text-gray-500">by {{ $impact['timescale'] }}</span>
                                    </div>
                                    <a href="#" wire:click="onRemoveProductivityImpact({{ $loop->index}})">
                                        <svg class="-ml-1 mr-0.5 flex-shrink-0 self-center h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                        </svg>
                                    </a>

                                </div>

                                <div class="mt-1 flex justify-between items-baseline md:block lg:flex">
                                    <div class="flex items-baseline text-2xl font-semibold text-red-600">
                                        {{ $impact['post'] }}
                                        <span class="ml-2 text-sm font-medium text-gray-500">
                                        from {{ $impact['pre'] }}
                                      </span>
                                    </div>

                                    <div class="inline-flex items-baseline px-2.5 py-0.5 rounded-full text-sm font-medium bg-green-100 text-green-800 md:mt-2 lg:mt-0">
                                        @if($impact['post'] < $impact['pre'])
                                            <svg class="-ml-1 mr-0.5 flex-shrink-0 self-center h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M14.707 10.293a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 111.414-1.414L9 12.586V5a1 1 0 012 0v7.586l2.293-2.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                            </svg>
                                        @else
                                            <svg class="-ml-1 mr-0.5 flex-shrink-0 self-center h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M5.293 9.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 7.414V15a1 1 0 11-2 0V7.414L6.707 9.707a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                            </svg>
                                        @endif

                                        <span class="sr-only">
                                            @if($impact['post'] > $impact['pre'])
                                                Increased by
                                            @else
                                                Decreased by
                                            @endif
          </span>

                                                {{ number_format(abs(1 - (filter_var($impact["post"], FILTER_SANITIZE_NUMBER_FLOAT) / filter_var($impact["pre"], FILTER_SANITIZE_NUMBER_FLOAT))) * 100) }}%



                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="shadow sm:rounded-md sm:overflow-hidden mt-4">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="impact_impact" value="{{ __('Productivity measure') }}" />
                                        <x-jet-input id="impact_impact" type="text" class="mt-1 block w-full" wire:model="productivityImpactForm.impact" />
                                        <x-jet-input-error for="productivityImpactForm.impact" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-1">
                                        <x-jet-label for="impact_pre" value="{{ __('Prior to project') }}" />
                                        <x-jet-input id="impact_pre" type="text" class="mt-1 block w-full" wire:model="productivityImpactForm.pre" />
                                        <x-jet-input-error for="productivityImpactForm.pre" class="mt-2" />
                                    </div>
                                    <div class="col-span-3 sm:col-span-1">
                                        <x-jet-label for="impact_post" value="{{ __('After implementation') }}" />
                                        <x-jet-input id="impact_post" type="text" class="mt-1 block w-full" wire:model="productivityImpactForm.post" />
                                        <x-jet-input-error for="productivityImpactForm.post" class="mt-2" />
                                    </div>
                                    <div class="col-span-3 sm:col-span-1">
                                        <x-jet-label for="impact_timescale" value="{{ __('Timescale') }}" />
                                        <x-jet-input id="impact_timescale" type="text" class="mt-1 block w-full" wire:model="productivityImpactForm.timescale" />
                                        <x-jet-input-error for="productivityImpactForm.timescale" class="mt-2" />
                                    </div>
                                </div>

                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" wire:click="onAddProductivityImpact">
                                    Add
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900 mb-2">Economic impact</h3>
                            <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lorem nisi, commodo posuere tincidunt nec, pulvinar quis arcu. Phasellus porttitor rutrum mattis.</p>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="mt-5 md:mt-0 md:col-span-2 p-4 sm:p-0">
                            @foreach($economicImpact as $impact)
                                <div class="rounded-lg bg-white overflow-hidden shadow divide-y divide-gray-200 md:grid-cols-3 md:divide-y-0 md:divide-x mb-4">
                                    <div class="px-4 py-5 sm:p-6">
                                        <div class="flex justify-between">
                                            <div class="text-base font-normal text-gray-900">
                                                {{ $impact['impact'] }} <span class="text-sm text-gray-500">by {{ $impact['timescale'] }}</span>
                                            </div>
                                            <a href="#">
                                                <svg class="-ml-1 mr-0.5 flex-shrink-0 self-center h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                                </svg>
                                            </a>

                                        </div>

                                        <div class="mt-1 flex justify-between items-baseline md:block lg:flex">
                                            <div class="flex items-baseline text-2xl font-semibold text-red-600">
                                                {{ $impact['value'] }}
                                            </div>

                                            @if(is_numeric($impact['percent_increase']))
                                            <div class="inline-flex items-baseline px-2.5 py-0.5 rounded-full text-sm font-medium bg-green-100 text-green-800 md:mt-2 lg:mt-0">

                                                    <svg class="-ml-1 mr-0.5 flex-shrink-0 self-center h-5 w-5 text-green-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                        <path fill-rule="evenodd" d="M5.293 9.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 7.414V15a1 1 0 11-2 0V7.414L6.707 9.707a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                                    </svg>

                                                <span class="sr-only">Increased by</span>

                                                    {{ $impact['percent_increase'] }}%

                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="shadow sm:rounded-md sm:overflow-hidden mt-4">
                                <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3">
                                            <x-jet-label for="about_project" value="{{ __('Impact type') }}" />
                                            <x-jet-input id="about_project" type="text" class="mt-1 block w-full" autocomplete="about_project" rows="4" />
                                            <x-jet-input-error for="about_project" class="mt-2" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="pre_project" value="{{ __('£/Numbers') }}" />
                                            <x-jet-input id="pre_project" type="text" class="mt-1 block w-full" autocomplete="proposed_supplier" />
                                            <x-jet-input-error for="pre_project" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="pre_project" value="{{ __('% Increase') }}" />
                                            <x-jet-input id="pre_project" type="text" class="mt-1 block w-full" autocomplete="proposed_supplier" />
                                            <x-jet-input-error for="pre_project" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="pre_project" value="{{ __('Timescale') }}" />
                                            <x-jet-input id="pre_project" type="text" class="mt-1 block w-full" autocomplete="proposed_supplier" />
                                            <x-jet-input-error for="pre_project" class="mt-2" />
                                        </div>
                                    </div>

                                </div>
                                <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Other potential impacts</h3>
                            <p class="text-sm text-gray-500">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lorem nisi, commodo posuere tincidunt nec, pulvinar quis arcu. Phasellus porttitor rutrum mattis.</p>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="about_project" value="Please enter any other potential impacts" class="mb-4" />
                                        <x-textarea id="about_project" type="text" class="mt-1 block w-full" autocomplete="about_project" rows="12" />
                                        <x-jet-input-error for="about_project" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

        </div>
    </div>
</div>

<div>
    <x-forms.header :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">What is your current Productivity Challenge?</h3>
                            <p class="mt-1 max-w-2xl text-sm text-gray-500 font-bold">Available Marks: 10</p>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="about_project" value="Clearly outline the need for the grant and how the investment is aligned to the companies overarching growth strategy." class="mb-4" />
                                        <x-textarea id="about_project" type="text" class="mt-1 block w-full" autocomplete="about_project" rows="12" />
                                        <x-jet-input-error for="about_project" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />
        </div>
    </div>
</div>

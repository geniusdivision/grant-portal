<div>
    <x-forms.header :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">How will the Productivity Grant improve productivity within your business and how will this be measured?</h3>
                            <p class="mt-1 max-w-2xl text-sm text-gray-500 font-bold">Available Marks: 10</p>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="about_project" value="Provide details of how you plan to show a suitable output to demonstrate the productivity improvement resulting from the proposed investment(s). The answer must clearly show how the activity will improve productivity of the business and how this will be measured." class="mb-4" />
                                        <x-textarea id="about_project" type="text" class="mt-1 block w-full" autocomplete="about_project" rows="24" />
                                        <x-jet-input-error for="about_project" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" class="mb-4" />

        </div>
    </div>
</div>

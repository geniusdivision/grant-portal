<div>
    <livewire:form-header :application="$application" :currentStep="$currentStep" :totalSteps="count($steps)" :navEnabled="$application->status != 'In progress'" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">What is the project?</h3>
                            <ul class="mt-1 max-w-2xl text-sm text-gray-500 list-disc">
                                <li class="mt-2">What is the business looking to acquire with the grant funding?</li>
                                <li class="mt-2">What is the overall objective of the project? - Explain as expressively as possible the ultimate, "big picture" vision and purpose of your completed endeavor.</li>
                                <li class="mt-2">Explain why grant funding is needed, with reference to the viability of the project with or without grant funding.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="business_problem" value="Your answer" class="mb-4 sr-only" />
                                        <x-textarea id="business_problem" type="text" class="mt-1 block w-full" autocomplete="business_problem" rows="12" wire:model="steps.{{ $stepKey }}.fields.businessProblem" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.businessProblem" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'business-problem'])
                    </div>
                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">How will this project help the growth of your business?</h3>
                            <ul class="mt-1 max-w-2xl text-sm text-gray-500 list-disc">
                                <li class="mt-2">Describe in detail the problem(s) currently being faced by the business.</li>
                                <li class="mt-2">Describe how the investment will resolve the problems and contribute to the development or creation of new products or services.</li>
                                <li class="mt-2">Evidence how the investment is aligned to the companies overarching growth strategy, referencing your ‘target market’ i.e. local, regional, national, international).</li>
                            </ul>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="digital_transformation" value="Your answer" class="mb-4 sr-only" />
                                        <x-textarea id="digital_transformation" type="text" class="mt-1 block w-full" autocomplete="digital_transformation" rows="12" wire:model="steps.{{ $stepKey }}.fields.digitalTransformation" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.digitalTransformation" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'digital-transformation'])
                    </div>
                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Who Will Benefit from your Project?</h3>
                            <p class="mt-1 max-w-2xl text-sm text-gray-500">Summarise the economic benefits outputs that the project will be projected to deliver. This could include:</p>
                            <ul class="mt-1 max-w-2xl text-sm text-gray-500 list-disc">
                                <li class="mt-2">Introducing new products, services or procedures</li>
                                <li class="mt-2">Accessing new markets</li>
                                <li class="mt-2">Safeguarding existing jobs</li>
                                <li class="mt-2">Creating New Job Opportunities</li>
                                <li class="mt-2">Increased Turnover</li>
                                <li class="mt-2">Increased Profit</li>
                            </ul>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="economic_impact" value="Your answer" class="mb-4 sr-only" />
                                        <x-textarea id="economic_impact" type="text" class="mt-1 block w-full" autocomplete="economic_impact" rows="12" wire:model="steps.{{ $stepKey }}.fields.economicImpact" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.economicImpact" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'wider-economic-impact'])


                    </div>
                </div>
            </div>

            <div class="md:grid md:grid-cols-3 md:gap-6">
                <div class="md:col-span-1">
                    <div class="px-4 sm:px-0">
                        <h3 class="text-lg font-medium leading-6 text-gray-900 mb-2">Economic impact</h3>
                        <p class="text-sm text-gray-500 mb-2">Please provide details of any expected economic impacts if your grant application is successful.</p>
                        <p class="text-sm text-gray-500 font-medium">Please enter n/a if a field is does not apply to your project.</p>
                    </div>
                </div>
                <div class="mt-5 md:mt-0 md:col-span-2">
                    <div class="mt-5 md:mt-0 md:col-span-2 p-4 sm:p-0 mb-4">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">

                            <div class="shadow sm:rounded-md sm:overflow-hidden">
                                <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3">
                                            <x-jet-label for="about_project" value="{{ __('Jobs created (FTEs)') }}" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="value" value="{{ __('Numbers') }}" />
                                            <x-jet-input :disabled="$disabled" id="value" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.jobsCreatedValue" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.jobsCreatedValue" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="increase" value="{{ __('% Increase') }}" />
                                            <x-jet-input :disabled="$disabled" id="increase" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.jobsCreatedIncrease" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.jobsCreatedIncrease" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="timescale" value="{{ __('Timescale') }}" />
                                            <x-jet-input :disabled="$disabled" id="timescale" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.jobsCreatedTimescale" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.jobsCreatedTimescale" class="mt-2" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3">
                                            <x-jet-label for="about_project" value="{{ __('Jobs safeguarded') }}" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="value" value="{{ __('Numbers') }}" />
                                            <x-jet-input :disabled="$disabled" id="value" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.jobsSafeguardedValue" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.jobsSafeguardedValue" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="increase" value="{{ __('% Increase') }}" />
                                            <x-jet-input :disabled="$disabled" id="increase" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.jobsSafeguardedIncrease" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.jobsSafeguardedIncrease" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="timescale" value="{{ __('Timescale') }}" />
                                            <x-jet-input :disabled="$disabled" id="timescale" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.jobsSafeguardedTimescale" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.jobsSafeguardedTimescale" class="mt-2" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3">
                                            <x-jet-label for="about_project" value="{{ __('Increased Turnover') }}" />

                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="value" value="{{ __('£') }}" />
                                            <x-jet-input :disabled="$disabled" id="value" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.increasedTurnoverValue" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.increasedTurnoverValue" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="increase" value="{{ __('% Increase') }}" />
                                            <x-jet-input :disabled="$disabled" id="increase" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.increasedTurnoverIncrease" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.increasedTurnoverIncrease" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="timescale" value="{{ __('Timescale') }}" />
                                            <x-jet-input :disabled="$disabled" id="timescale" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.increasedTurnoverTimescale" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.increasedTurnoverTimescale" class="mt-2" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3">
                                            <x-jet-label for="about_project" value="{{ __('Increased Profit') }}" />

                                        </div>
                                    </div>
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="value" value="{{ __('£') }}" />
                                            <x-jet-input :disabled="$disabled" id="value" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.increasedProfitValue" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.increasedProfitValue" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="increase" value="{{ __('% Increase') }}" />
                                            <x-jet-input :disabled="$disabled" id="increase" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.increasedProfitIncrease" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.increasedProfitIncrease" class="mt-2" />
                                        </div>
                                        <div class="col-span-3 sm:col-span-1">
                                            <x-jet-label for="timescale" value="{{ __('Timescale') }}" />
                                            <x-jet-input :disabled="$disabled" id="timescale" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.increasedProfitTimescale" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.increasedProfitTimescale" class="mt-2" />
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <x-jet-input-error for="economicImpact" class="mt-2" />
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'economic-impact'])
                    </div>
                </div>

            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Can you explain the rationale behind your chosen supplier/provider, demonstrating the chosen supplier’s experience and/or value for money?</h3>
                            <ul class="mt-1 max-w-2xl text-sm text-gray-500 list-disc">
                                <li class="mt-2">Satisfy that the proposals are viable, offer value for money, are realistic and deliverable</li>
                                <li class="mt-2">Highlight which supplier(s) you have approached and why.</li>
                                <li class="mt-2">Detail the quotes received and the reason why a specific supplier was chosen. If you have an existing relationship with the supplier, please state what this is.</li>
                                <li class="mt-2">If only one supplier has been approached explain why.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="chosen_supplier" value="Your answer" class="mb-4 sr-only" />
                                        <x-textarea id="chosen_supplier" type="text" class="mt-1 block w-full" autocomplete="chosen_supplier" rows="12" wire:model="steps.{{ $stepKey }}.fields.chosenSupplier" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.chosenSupplier" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'chosen-supplier'])
                    </div>
                </div>
            </div>

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" :application="$application" class="mb-4" />

        </div>
    </div>
</div>

<div>
    <livewire:form-header :application="$application" :currentStep="$currentStep" :totalSteps="count($steps)" :navEnabled="$application->status != 'In progress'" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Confirmations</h3>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white sm:p-6">

                                <div class="relative flex items-start mt-2 mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="privacy" name="privacy" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.privacy" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="privacy" class="font-medium text-gray-700 cursor-pointer">Privacy Policy</label>
                                        <p class="text-gray-500">I/We have read and accept the <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-privacy-policy/" target="_blank" class="text-red-600">Privacy Policy</a> and consent to usage of the information provided for the purposes described.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.privacy" class="mt-2" />
                                    </div>
                                </div>



                                <div class="relative flex items-start mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="data" name="data" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.data" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="data" class="font-medium text-gray-700 cursor-pointer">Data Usage Statement</label>
                                        <p class="text-gray-500">I/We have read and accept the <a href="{{ route('applications.data',$application->applicationForm) }}" target="_blank" class="text-red-600">Data Usage Statement</a> and consent to usage of the information provided for the purposes described.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.data" class="mt-2" />
                                    </div>
                                </div>

                                <div class="relative flex items-start mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="authorised" name="authorised" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.authorised" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="authorised" class="font-medium text-gray-700 cursor-pointer">Authorisation</label>
                                        <p class="text-gray-500">I/We confirm that I/We are authorised to submit this application on behalf og the business and confirm that I/We understand the requirements of De Minimis and acknowledge that if the business fails to meet the eligibility requirements it may become liable to repay the full value of assistance provided.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.authorised" class="mt-2" />
                                    </div>
                                </div>

                                <div class="relative flex items-start mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="terms" name="terms" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.terms" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="terms" class="font-medium text-gray-700 cursor-pointer">Terms and Conditions</label>
                                        <p class="text-gray-500">I/We agree to comply with the <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-terms-conditions/" target="_blank" class="text-red-600">Terms and Conditions</a> of the application and any associated guidance documents.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.terms" class="mt-2" />
                                    </div>
                                </div>

                                <div class="relative flex items-start mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="future_participation_not_required" name="future_participation_not_required" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.future_participation_not_required" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="future_participation_not_required" class="font-medium text-gray-700 cursor-pointer">Future Support</label>
                                        <p class="text-gray-500">I/We understand that completion of this form neither entitles nor requires the business to take part in any particular business support services in the future.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.future_participation_not_required" class="mt-2" />
                                    </div>
                                </div>

                                <div class="relative flex items-start mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="document_retention" name="document_retention" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.document_retention" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="document_retention" class="font-medium text-gray-700 cursor-pointer">Document Retention</label>
                                        <p class="text-gray-500">I/We acknowledge the document retention requirement as detailed in the Application Guidance and confirm I/We will retain the original document until further notice.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.document_retention" class="mt-2" />
                                    </div>
                                </div>

                                <div class="relative flex items-start mb-4">
                                    <div class="flex items-center h-5">
                                        <x-jet-checkbox id="accuracy" name="accuracy" class="focus:ring-red-500 h-4 w-4 text-red-600 border-gray-300 rounded" wire:model="steps.{{ $stepKey }}.fields.accuracy" value="1" :disabled="$disabled" />
                                    </div>
                                    <div class="ml-3 text-sm">
                                        <label for="accuracy" class="font-medium text-gray-700 cursor-pointer">Accuracy of details</label>
                                        <p class="text-gray-500">The details provided on this form are current and accurate to the best of my/our knowledge.</p>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.accuracy" class="mt-2" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'confirmations'])
                    </div>
                </div>
            </div>
            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" :application="$application" class="mb-4" />
        </div>
    </div>
</div>

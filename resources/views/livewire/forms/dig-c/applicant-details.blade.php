<div>
    <livewire:form-header :application="$application" :currentStep="$currentStep" :totalSteps="count($steps)" :navEnabled="$application->status != 'In progress'" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div>
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Applicant Business Details</h3>
                        </div>
                    </div>

                    <div class="mt-5 md:mt-0 md:col-span-2">
                            <div class="shadow sm:rounded-md sm:overflow-hidden">
                                <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div class="grid grid-cols-6 gap-6">
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="business_name" value="{{ __('Business Name') }}" />
                                            <x-jet-input id="business_name" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.businessName" value="Test" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.businessName" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="company_position" value="{{ __('Company Position') }}" />
                                            <x-jet-input id="company_position" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.companyPosition" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.companyPosition" class="mt-2" />
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="address_1" value="{{ __('Business Address Line 1') }}" />
                                            <x-jet-input id="address_1" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.address1" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.address1" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="address_2" value="{{ __('Business Address Line 2') }}" />
                                            <x-jet-input id="address_2" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.address2" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.address2" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="address_city" value="{{ __('Town/City') }}" />
                                            <x-jet-input id="address_city" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.city" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.city" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="postcode" value="{{ __('Postcode') }}" />
                                            <x-jet-input id="postcode" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.postcode" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.postcode" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="telephone" value="{{ __('Telephone') }}" />
                                            <x-jet-input id="telephone" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.telephone" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.telephone" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="mobile" value="{{ __('Mobile') }}" />
                                            <x-jet-input id="mobile" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.mobile" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.mobile" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="email" value="{{ __('Email Address') }}" />
                                            <x-jet-input id="email" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.email" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.email" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="website" value="{{ __('Website') }}" />
                                            <x-jet-input id="website" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.website" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.website" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-jet-label for="business_size" value="{{ __('Business Size') }}" />
                                            <x-select id="business_size" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.businessSize" :options="[
                                                'Sole Trader' => 'Sole Trader',
                                                'Micro Business' => 'Micro Business',
                                                'SME' => 'SME',
                                                'Large Business' => 'Large Business',
                                            ]" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.businessSize" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-jet-label for="business_type" value="{{ __('Business Type') }}" />
                                            <x-select id="business_type" type="text" class="mt-1 block w-full" wire:model="steps.{{ $stepKey }}.fields.businessType" :options="[
                                                'Limited Company' => 'Limited Company',
                                                'Charity' => 'Charity',
                                                'Partnership' => 'Partnership',
                                                'Self Employed' => 'Self Employed',
                                            ]" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.businessType" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-jet-label for="trading_since" value="{{ __('Trading Since') }}" />
                                            <x-jet-input id="trading_since" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.tradingStartDate" placeholder="dd/mm/yyyy" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.tradingStartDate" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 flex items-start">
                                            <div class="flex items-center h-5">
                                                <x-jet-checkbox  name="social_enterprise" value="1" wire:model.defer="steps.{{ $stepKey }}.fields.socialEnterprise" />
                                            </div>
                                            <div class="ml-3 text-sm">
                                                <label for="social_enterprise" class="font-medium text-gray-700">Social Enterprise</label>
                                                <p class="text-gray-500">This business is a social enterprise</p>
                                            </div>
                                        </div>
                                        @if($steps[$stepKey]["fields"]["businessType"] != "Self Employed")
                                        <div class="col-span-6 sm:col-span-3">

                                            <x-jet-label for="company_number" >
                                                {{$steps[$stepKey]["fields"]["businessType"]}} {{ __('Registration Number') }}
                                            </x-jet-label>

                                            <x-jet-input id="company_number" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.companyNumber" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.companyNumber" class="mt-2" />
                                        </div>
                                        @else
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="company_urn" value="{{ __('Sole Trader UTR') }}" />
                                            <x-jet-input id="company_urn" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.soleTraderUTR" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.soleTraderUTR" class="mt-2" />
                                        </div>
                                        @endif
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="business_sector" value="{{ __('Business Sector') }}" />
                                            <x-select id="business_sector" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.businessSector"  :disabled="$disabled">
                                                @foreach(\App\Models\Sector::dropdownOptions() as $sector)
                                                <optgroup label="{{ $sector["name"] }}">
                                                    @foreach($sector["subsectors"] as $subsector)
                                                    <option value="{{ $subsector->id }}">{{ $subsector->name }}</option>
                                                    @endforeach
                                                </optgroup>
                                                @endforeach
                                            </x-select>
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.businessSector" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="company_location" value="{{ __('Registered in') }}" />
                                            <x-select id="company_location"  class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.registeredLocation" :options="[
                                                'England & Wales' => 'England & Wales',
                                                'Scotland' => 'Scotland',
                                                'Northern Ireland' => 'Northern Ireland',
                                                'Other' => 'Other',
                                            ]" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.registeredLocation" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-3">
                                            <x-jet-label for="vat_number" value="{{ __('VAT Number') }}" />
                                            <x-jet-input id="vat_number" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.vatNumber" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.vatNumber" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-jet-label for="local_employees" value="{{ __('Number of Local FTEs') }}" />
                                            <x-jet-input id="local_employees" type="number" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.localEmployees" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.localEmployees" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-jet-label for="national_employees" value="{{ __('Number of National FTEs') }}" />
                                            <x-jet-input id="national_employees" type="number" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.nationalEmployees" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.nationalEmployees" class="mt-2" />
                                        </div>
                                        <div class="col-span-6 sm:col-span-2">
                                            <x-jet-label for="global_employees" value="{{ __('Number of Global FTEs') }}" />
                                            <x-jet-input id="global_employees" type="number" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.globalEmployees" :disabled="$disabled" />
                                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.globalEmployees" class="mt-2" />
                                        </div>
                                    </div>
                                    <p class="text-sm text-gray-500">FTE means full-time equivalent, so a full-time employee/volunteer (30 hours or more per week) counts as one unit, and a part-time employee counts as a fraction of one unit.</p>

                                </div>

                            </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'business-details'])
                    </div>
                </div>
            </div>



            @if($steps[$stepKey]['fields']['businessType'] != 'Self Employed' )

                <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

                <div class="mt-10 sm:mt-0" >
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">
                                @if($steps[$stepKey]['fields']['businessType'] == 'Limited Company')
                                    Company Directors
                                @else
                                    Senior Leadership Team
                                @endif
                            </h3>
                            <p class="mt-1 text-sm text-gray-600">
                                @if($steps[$stepKey]['fields']['businessType'] == 'Limited Company')
                                    We will need to know the names and titles of each of your directors.
                                @elseif($steps[$stepKey]['fields']['businessType'] == 'Charity')
                                    We will need to know the name of your trustees and anyone on your board.
                                @else
                                    We will need to know the names and of everyone in the partnership.
                                @endif

                            </p>
                            <x-jet-input-error for="steps.{{ $stepKey }}.fields.directors" class="mt-2" />

                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        @foreach($directors as $director)
                            <div class="rounded-lg bg-white overflow-hidden shadow divide-y divide-gray-200 md:grid-cols-3 md:divide-y-0 md:divide-x mb-4">
                                <div class="px-4 py-5 sm:p-6">
                                    <div class="flex justify-between items-center">
                                        <div class="text-base font-normal text-gray-900 flex justify-between w-full items-center pr-4">
                                            <div>{{ $director['name'] }}</div>
                                            <span class="text-gray-500">{{ $director['position'] }}</span>
                                        </div>
                                        @if(!$disabled)
                                        <a href="#" wire:click.prevent="onRemoveDirector({{ $loop->index}})" class="w-5 h-5 cursor-pointer">
                                            <svg class=" mr-0.5 flex-shrink-0 self-center h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                            </svg>
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @if(!$disabled)
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3">
                                        <x-jet-label for="director_name" value="{{ __('Name') }}" />
                                        <x-jet-input id="director_name" type="text" class="mt-1 block w-full" wire:model.defer="directorFields.name" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.directorFields.name" class="mt-2" />
                                    </div>
                                    <div class="col-span-3">
                                        <x-jet-label for="director_position" value="{{ __('Position') }}" />
                                        <x-jet-input id="director_position" type="text" class="mt-1 block w-full" wire:model.defer="directorFields.position" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.directorFields.position" class="mt-2" />
                                    </div>
                                </div>

                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6 flex items-center justify-between">
                                <div>
                                    <x-jet-input-error for="directors"/>
                                </div>

                                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" wire:click="onAddDirector">
                                    Save Person
                                </button>
                            </div>


                        </div>
                        @endif
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'directors'])
                    </div>
                </div>
            </div>

            @endif



            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="md:grid md:grid-cols-3 md:gap-6">
                <div class="md:col-span-1">
                    <div class="px-4 sm:px-0">
                        <h3 class="text-lg font-medium leading-6 text-gray-900 mb-2">Statement of Previous Aid</h3>
                        <p class="text-sm text-gray-500">The Digital Innovation Grant project is part funded by the European Regional Development Fund as part of the European Structural and Investment Funds Growth Programme 2014-2020.  Financial and non-financial support given to your company as part of this project is categorised as ‘De Minimis Aid, Commission Regulation (EU) No 1407/2013’.  This allows a single undertaking to receive up to €200,000 of De Minimis Aid over a rolling three-year period comprising the current and previous two financial years. If your business is part of a group this aid ceiling may be cumulative; please discuss with your Advisor if you believe this applies to you.</p>
                        <p class="text-sm text-gray-500 mt-2"><strong>It is your responsibility to check whether funds received are ‘De Minimis’. If in doubt, please check with the funding sources.</strong></p>
                    </div>
                </div>
                <div class="mt-5 md:mt-0 md:col-span-2 p-4 sm:p-0">
                    @foreach($previousAid as $aid)
                        <div class="rounded-lg bg-white overflow-hidden shadow divide-y divide-gray-200 md:grid-cols-3 md:divide-y-0 md:divide-x mb-4">
                            <div class="px-4 py-5 sm:p-6">
                                <div class="flex justify-between items-center">
                                    <div class="grid grid-cols-2 sm:grid-cols-4 gap-4 w-full">
                                        <div class="text-base font-bold text-red-500">{{ money($aid['amount']*100, 'GBP') }}</div>
                                        <div class="text-base font-normal text-gray-900">{{ $aid['date'] }}</div>
                                        <div class="text-base text-gray-500"> {{ $aid['body'] }}</div>
                                        <div class="text-base text-gray-500"> {{ $aid['purpose'] }}</div>
                                    </div>

                                    @if(!$disabled)
                                        <a href="#" wire:click.prevent="onRemoveAid({{ $loop->index}})" class="w-5 h-5 cursor-pointer">
                                            <svg class=" mr-0.5 flex-shrink-0 self-center h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                            </svg>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if(!$disabled)
                        <div class="shadow sm:rounded-md sm:overflow-hidden mt-4">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <p class="text-gray-900 font-medium">Please enter the details of any public funding your business has received in the last three years.</p>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-1">
                                        <x-jet-label for="previous_aid_date" value="{{ __('Date funding granted') }}" />
                                        <x-jet-input id="previous_aid_date" type="text" class="mt-1 block w-full" wire:model="previousAidFields.date" placeholder="mm/yyyy" />
                                        <x-jet-input-error for="previousAidFields.date" class="mt-2" />
                                    </div>
                                    <div class="col-span-3 sm:col-span-1">
                                        <x-jet-label for="previous_aid_funding_body" value="{{ __('Funding Body') }}" />
                                        <x-jet-input id="impact_pre" type="text" class="mt-1 block w-full" wire:model="previousAidFields.body" />
                                        <x-jet-input-error for="previousAidFields.body" class="mt-2" />
                                    </div>
                                    <div class="col-span-3 sm:col-span-1">
                                        <x-jet-label for="previous_aid_amount" value="{{ __('Amount') }}" />
                                        <x-input-group id="previous_aid_amount" type="number" div-class="mt-1" addon="£" wire:model="previousAidFields.amount"></x-input-group>
                                        <x-jet-input-error for="previousAidFields.amount" class="mt-2" />
                                    </div>
                                    <div class="col-span-3">
                                        <x-jet-label for="previous_aid_purpose" value="{{ __('Purpose') }}" />
                                        <x-jet-input id="previous_aid_purpose" type="text" class="mt-1 block w-full" wire:model="previousAidFields.purpose" />
                                        <x-jet-input-error for="previousAidFields.purpose" class="mt-2" />
                                    </div>
                                </div>

                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" wire:click="onAddAid">
                                    Add
                                </button>
                            </div>
                        </div>
                    @endif
                    @livewire('application-notes',[ 'application'=>$application, 'key'=>'previous-aid'])
                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Project details</h3>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="estimated_start_date" value="{{ __('Estimated start date') }}" />
                                        <x-jet-input id="estimated_start_date" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.projectStartDate" autocomplete="estimated_start_date" placeholder="dd/mm/yyyy" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.projectStartDate" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="estimated_end_date" value="{{ __('Estimated end date') }}" />
                                        <x-jet-input id="estimated_end_date" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.projectEndDate" autocomplete="estimated_end_date" placeholder="dd/mm/yyyy" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.projectEndDate" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="total_project_cost" value="{{ __('Total project cost') }}" />
                                        <x-input-group id="total_project_cost" type="number" addon="£" wire:model.defer="steps.{{ $stepKey }}.fields.projectCost" :disabled="$disabled"></x-input-group>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.projectCost" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="total_grant_requested" value="{{ __('Total grant requested') }}" />
                                        <x-input-group id="total_grant_requested" type="number" addon="£" wire:model.defer="steps.{{ $stepKey }}.fields.grantRequested" :disabled="$disabled"></x-input-group>
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.grantRequested" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="proposed_supplier" value="{{ __('Proposed supplier') }}" />
                                        <x-jet-input id="proposed_supplier" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.proposedSupplier" autocomplete="proposed_supplier" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.proposedSupplier" class="mt-2" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <x-jet-label for="about_project" value="{{ __('About your project') }}" />
                                        <x-textarea id="about_project" type="text" class="mt-1 block w-full" wire:model.defer="steps.{{ $stepKey }}.fields.aboutProject" autocomplete="about_project" rows="12" :disabled="$disabled" />
                                        <x-jet-input-error for="steps.{{ $stepKey }}.fields.aboutProject" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire('application-notes',[ 'application'=>$application, 'key'=>'project-details'])
                    </div>
                </div>
            </div>
            @if(count($errors))
                <p class="text-sm text-red-600 mt-8 text-right">Some information is missing. Please ensure all form fields are filled out correctly before continuing.</p>
            @endif

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" :application="$application" class="mb-4" />

        </div>
    </div>
</div>

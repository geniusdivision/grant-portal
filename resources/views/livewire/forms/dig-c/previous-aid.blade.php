<div>
    <livewire:form-header :application="$application" :currentStep="$currentStep" :totalSteps="count($steps)" :navEnabled="$application->status != 'In progress'" class="mb-4" />

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div>

            </div>

            <x-forms.footer :name="$name" :currentStep="$currentStep" :totalSteps="count($steps)" :application="$application" class="mb-4" />

        </div>
    </div>
</div>

<div class="py-4 sm:py-12">


    <div class="grid lg:grid-cols-4 max-w-7xl mx-auto sm:px-6 lg:px-8 gap-6 lg:gap-8">
        <div class="lg:col-span-3 order-2 lg:order-1">
            <div class="shadow sm:rounded-md sm:overflow-hidden">
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="w-full">
                        <div class="text-sm pr-5 mb-4">
                            <x-jet-label>
                                Does your business have its registered and/or trading office address in any of the following local authorities? &lsquo;Virtual&rsquo; tenancy agreements <strong>do not</strong> qualify.
                            </x-jet-label>
                        </div>
                        <div class="">
                            <div class="flex items-center mb-1">
                                <x-jet-checkbox name="business_rates_barnsley" id="business_rates_barnsley" wire:model="businessRates.0" value="Barnsley" />
                                <x-jet-label for="business_rates_barnsley" class="ml-3">Barnsley</x-jet-label>
                            </div>
                            <div class="flex items-center mb-1">
                                <x-jet-checkbox name="business_rates_doncaster" id="business_rates_doncaster" wire:model="businessRates.1" value="Doncaster" />
                                <x-jet-label for="business_rates_doncaster" class="ml-3">Doncaster</x-jet-label>
                            </div>
                            <div class="flex items-center mb-1">
                                <x-jet-checkbox name="business_rates_rotherham" id="business_rates_rotherham" wire:model="businessRates.2" value="Rotherham" />
                                <x-jet-label for="business_rates_rotherham" class="ml-3">Rotherham</x-jet-label>
                            </div>
                            <div class="flex items-center mb-1">
                                <x-jet-checkbox name="business_rates_sheffield" id="business_rates_sheffield" wire:model="businessRates.3" value="Sheffield" />
                                <x-jet-label for="business_rates_sheffield" class="ml-3">Sheffield</x-jet-label>
                            </div>

                        </div>
                    </div>
                </div>

                @if(in_array("Barnsley",$businessRates))
                    <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                        <div class="relative flex items-center w-full">
                            <div class="flex items-center h-5">
                                <x-jet-checkbox name="barnsley_only" id="barnsley_employee_promise_initiative" wire:model="barnsleyEmployeePromiseInitiative" />
                            </div>
                            <div class="ml-3 text-sm pr-5">
                                <x-jet-label for="barnsley_employee_promise_initiative">
                                    Will you participate in Barnsley Council’s <a href="https://www.barnsley.gov.uk/services/business-information/more-and-better-jobs-employer-promise/">Employer Promise Initiative</a>?
                                </x-jet-label>
                            </div>

                        </div>
                    </div>
                @endif
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex items-top w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="business_size" id="business_size" wire:model="businessSize" />
                        </div>
                        <div class="ml-3 text-sm pr-5">
                            <x-jet-label for="business_size">
                                Is your business categorised as a Small to Medium Sized Enterprise (SME)?
                            </x-jet-label>
                            <ul class="list-disc pl-5 mt-2  text-gray-600">
                                <li>Your business has fewer than 250 full-time equivalent (FTE) employees or volunteers.</li>
                                <li>Your business has an annual turnover not exceeding €50m or an annual balance sheet total not exceeding €43m.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex items-center w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="business_age" id="business_age" wire:model="businessAge" />
                        </div>
                        <div class="ml-3 text-sm pr-5">
                            <x-jet-label for="business_age">
                                Has your business been established for at least 12 months? If not, will you complete additional verification checks to examine the credentials and financial viability of your business?
                            </x-jet-label>
                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="business_to_business" id="business_to_business" wire:model="businessToBusiness" />
                        </div>
                        <div class="ml-3 text-sm pr-5">

                            <x-jet-label for="business_to_business">
                                <p class="mb-2">Does your business generate the majority of its turnover from trading with other businesses, or, will your application demonstrate how the grant supported project will lead to this?</p>
                                <p>OR: If your business generates the majority of its turnover from trading with consumers (eg. retail), can you confirm you serve customers outside of the 'local area' (eg. online retail), and/or that your project will support this?</p>
                            </x-jet-label>
                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="finance_available" id="finance_available" wire:model="financeAvailable" />
                        </div>
                        <div class="ml-3 text-sm pr-5">
                            <x-jet-label for="finance_available">
                                Does your business have private finance available, either your own, from a bank or other commercial finance to invest alongside the grant?
                            </x-jet-label>
                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex items-center w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="financial_viability" id="financial_viability" wire:model="financialViability" />
                        </div>
                        <div class="ml-3 text-sm pr-5">
                            <x-jet-label for="financial_viability">
                                Can you demonstrate financial viability of the project and the need for grant support?
                            </x-jet-label>
                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex  w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="restricted_sectors" id="restricted_sectors" wire:model="restrictedSectors" />
                        </div>
                        <div class="ml-3 text-sm pr-5">
                            <x-jet-label for="restricted_sectors">
                                Can you confirm that your business / project will not be engaged in the following industries / activities?
                            </x-jet-label>

                            <ul class="list-disc pl-5 mt-2  text-gray-600">
                                <li>Retailing or providing localised business to consumer services.</li>
                                <li>Day Nurseries</li>
                                <li>Fishery & Aquaculture</li>
                                <li>Primary Production</li>
                                <li>Synthetic fibres / textiles & clothing</li>
                                <li>Shipbuilding</li>
                                <li>Coal & Steel</li>
                                <li>Banking and Insurance</li>
                                <li>Generalised school age education</li>
                                <li>Processing and marketing or agricultural products.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex items-center  w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="state_aid" id="state_aid" wire:model="stateAid" />
                        </div>
                        <div class="ml-3 text-sm pr-5">

                            <x-jet-label for="state_aid">
                                Can you confirm your business has not received more than €200,000 of state aid in the last 3 fiscal years?
                            </x-jet-label>

                        </div>

                    </div>
                </div>
                <div class="px-4 py-5 bg-white space-y-6 border-t border-gray-200 sm:pt-5">
                    <div class="relative flex items-center w-full">
                        <div class="flex items-center h-5">
                            <x-jet-checkbox name="eb-funding" id="previous_eb_funding" wire:model="ebFunding" />
                        </div>
                        <div class="ml-3 text-sm pr-5">
                            <x-jet-label for="previous_eb_funding">
                                Can you confirm that your business has not already received grant funding for this project, either from Enterprising Barnsley (e.g. Business Productivty, Restart & Recovery, Kickstarting Tourism, and other grants) or other bodies?
                            </x-jet-label>
                        </div>

                    </div>
                </div>
            </div>
            @if(count($errors))
                <div class="px-4 pt-4 space-y-6 border-t border-gray-200 sm:pt-5">
                    <p class="text-sm text-red-600 sm:text-right">You must meet all of the eligibility criteria in order to apply. Please <a href="mailto:digitalinnovationgrants@barnsley.gov.uk" class="underline">email us</a> or <a href="https://www.enterprisingbarnsley.co.uk/contact/" class="underline">contact us</a> if you have any questions about eligibility.</p>
                </div>
            @endif
            <div class="flex justify-end mt-4 pr-2 sm:pr-0">
                <button wire:click="onProceed" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                    Continue to application
                </button>
            </div>
        </div>
        <div class="col-span-1 text-sm text-gray-600 p-4 lg:p-0 order-1 lg:order-2">

            <h2 class="text-gray-700 mb-4 font-bold text-lg">
                Eligibility Criteria Guidance
            </h2>

            <p class="mb-4">After contacting the appropriate Local Authority / Growth Hub business advisor, the first step to funding success is to check your eligibility.</p>

            <p class="mb-4">You must demonstrate that your business/ project meets all the eligibility criteria. Any projects that do not meet all the eligibility criteria will not progress to stage 2 - Productivity Grant Application.</p>

            <p class="mb-4">If you feel that you are unable to meet all eligibility requirements it is still worth discussing with the relevant Local Authority or the SCR Growth hub if there are other opportunities where your business can benefit from wider support available across the SCR.</p>
{{--            <p class=""><a href="{{ route('applications.help') }}" class="text-red-600 underline">Get help</a></p>--}}
            @if($applicationForm->guidance_document)
                <a href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-grant-guidance/" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 mr-4" target="_blank">
                    Download guidance document
                </a>
            @endif

        </div>
    </div>
</div>

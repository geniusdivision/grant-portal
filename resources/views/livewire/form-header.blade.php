<header class="bg-white shadow">
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">

        <div class="sm:flex justify-between items-center">
            <div class="mb-4 sm:mb-0">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ $application->applicationForm->name }} Application
                </h2>
                <div class="sm:flex">
                    <p class="text-xs mt-2 text-gray-600">Ref: <span class="font-bold text-gray-800">{{ $application->reference }}</span> </p>
                    @if($application->applicationForm->guidance_document)
                    <p class="text-xs mt-2 text-gray-600 sm:ml-4">Need help? <a class="underline" href="https://www.enterprisingbarnsley.co.uk/digital-innovation-grants/dig-grant-guidance/" target="_blank">Download guidance document</a></p>
                    @endif
                </div>


            </div>
            <nav class="flex sm:items-center justify-between sm:justify-center mt-2 sm:mt-0 text-gray-500" aria-label="Progress">
                <p class="text-sm font-medium">Step {{ $currentStep }} of {{ $totalSteps }}</p>
                <ol class="ml-8 flex items-center space-x-5">

                    @for ($i = 1; $i <= $totalSteps; $i++)
                        @if($currentStep > $i)
                            <li>
                                <!-- Completed Step -->
                                @if($navEnabled)
                                <a href="#" wire:click.prevent="onNavigate({{$i}})" class="block w-2.5 h-2.5 bg-red-600 rounded-full">
                                    <span class="sr-only">Step 1</span>
                                </a>
                                @else
                                    <div class="block w-2.5 h-2.5 bg-red-600 rounded-full">
                                        <span class="sr-only">Step 1</span>
                                    </div>
                                @endif
                            </li>
                        @elseif($currentStep == $i)
                            <li>

                                <!-- Current Step -->
                                @if($navEnabled)
                                <a href="#" wire:click.prevent="onNavigate({{$i}})" class="relative flex items-center justify-center" aria-current="step">
        <span class="absolute w-5 h-5 p-px flex" aria-hidden="true">
          <span class="w-full h-full rounded-full bg-red-200"></span>
        </span>
                                    <span class="relative block w-2.5 h-2.5 bg-red-600 rounded-full" aria-hidden="true"></span>
                                    <span class="sr-only">Step {{ $i }}</span>
                                </a>
                                @else
                                    <div class="relative flex items-center justify-center" aria-current="step">
        <span class="absolute w-5 h-5 p-px flex" aria-hidden="true">
          <span class="w-full h-full rounded-full bg-red-200"></span>
        </span>
                                        <span class="relative block w-2.5 h-2.5 bg-red-600 rounded-full" aria-hidden="true"></span>
                                        <span class="sr-only">Step {{ $i }}</span>
                                    </div>
                                @endif
                            </li>
                        @else
                            <li>
                                <!-- Upcoming Step -->
                                @if($navEnabled)

                                    <a href="#" wire:click.prevent="onNavigate({{$i}})" class="block w-2.5 h-2.5 bg-gray-200 rounded-full">
                                        <span class="sr-only">Step {{ $i }}</span>
                                    </a>
                                @else
                                    <div class="block w-2.5 h-2.5 bg-gray-200 rounded-full">
                                        <span class="sr-only">Step {{ $i }}</span>
                                    </div>
                                @endif
                            </li>
                        @endif
                    @endfor

                </ol>
            </nav>
        </div>
    </div>
</header>

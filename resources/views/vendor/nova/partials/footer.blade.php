<p class="mt-8 text-center text-xs text-80">
    © {{ date('Y') }} Enterprising Barnsley
    <span class="px-1">&middot;</span>
    Built by <a href="https://www.geniusdivision.com" class="text-primary dim no-underline">Genius Division</a>


</p>

<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
    <div class="mx-20 mb-6">
        {{ $logo }}
    </div>
    <div class="w-full sm:max-w-md">
        <nav class="relative  rounded-lg shadow flex divide-x divide-gray-200 " aria-label="Tabs">
            <!-- Current: "text-gray-900", Default: "text-gray-500 hover:text-gray-700" -->
            <a href="/login" class="text-gray-900 rounded-l-lg group relative min-w-0 flex-1 overflow-hidden bg-white py-4 px-4 text-sm font-medium text-center hover:bg-gray-50 focus:z-10" aria-current="page">
                <span>Login</span>
                <span aria-hidden="true" class="{{Route::is('login') ? 'bg-red-500' : 'bg-transparent'}} absolute inset-x-0 bottom-0 h-0.5"></span>
            </a>

            <a href="/register" class="text-gray-500 hover:text-gray-700 rounded-r-lg group relative min-w-0 flex-1 overflow-hidden bg-white py-4 px-4 text-sm font-medium text-center hover:bg-gray-50 focus:z-10">
                <span>Register</span>
                <span aria-hidden="true" class="{{Route::is('register') ? 'bg-red-500' : 'bg-transparent'}} absolute inset-x-0 bottom-0 h-0.5"></span>
            </a>
        </nav>
    </div>


    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>

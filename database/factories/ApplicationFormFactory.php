<?php

namespace Database\Factories;

use App\Models\ApplicationForm;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApplicationFormFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ApplicationForm::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'description' => $this->faker->realText(),
            'privacy_policy' => $this->faker->realText(),
            'data_usage_statement' => $this->faker->realText(),
            'terms' => $this->faker->realText(),
        ];
    }
}

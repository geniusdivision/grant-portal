<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RoleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'view applications']);
        Permission::create(['name' => 'create applications']);
        Permission::create(['name' => 'update applications']);
        Permission::create(['name' => 'delete applications']);

        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'delete users']);

        Permission::create(['name' => 'review applications']);
        Permission::create(['name' => 'assign applications']);

        $client = Role::create(['name'=>'Client']);
        $client->givePermissionTo('view applications');
        $client->givePermissionTo('create applications');
        $client->givePermissionTo('update applications');
        $client->givePermissionTo('delete applications');

        $projectOfficer = Role::create(['name'=>'Project Officer']);
        $projectOfficer->givePermissionTo('review applications');
        $projectOfficer->givePermissionTo('view applications');
        $projectOfficer->givePermissionTo('update applications');

        $manager = Role::create(['name'=>'Manager']);
        $manager->givePermissionTo('view applications');
        $manager->givePermissionTo('update applications');
        $manager->givePermissionTo('delete applications');
        $manager->givePermissionTo('assign applications');
        $manager->givePermissionTo('review applications');
        $manager->givePermissionTo('view users');
        $manager->givePermissionTo('create users');
        $manager->givePermissionTo('update users');
        $manager->givePermissionTo('delete users');

        Role::create(['name'=>'Developer']);
    }
}

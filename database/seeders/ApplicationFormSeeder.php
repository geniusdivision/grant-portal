<?php

namespace Database\Seeders;

use App\Models\ApplicationForm;
use Illuminate\Database\Seeder;

class ApplicationFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApplicationForm::factory()->create();
    }
}

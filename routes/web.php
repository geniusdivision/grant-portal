<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});


Route::middleware(['auth:sanctum', 'verified'])->prefix('assignments')->group(function () {
    Route::middleware('can:review applications')->group(function(){
        Route::get('/','App\Http\Controllers\ApplicationController@assignments')->name('assignments');

        Route::middleware('projectOfficer')->get('review/{application}',function (\App\Models\Application $application){
            return view('applications.review',[
                'application' => $application,
            ]);
        })->name('applications.review');
    });

});

Route::middleware(['auth:sanctum', 'verified'])->prefix('applications')->group(function () {
    Route::middleware('can:create applications')->group(function(){
        Route::get('/','App\Http\Controllers\ApplicationController@index')->name('applications');

//        Route::view('help','applications.help')->name('applications.help');

        Route::get('apply/{form}/about',function(\App\Models\ApplicationForm $form){
            return view('applications.about.index',[
                'applicationForm' => $form,
            ]);
        })->middleware('applicationWindowClosed')->name('applications.about');

//        Route::get('apply/{form}/guidance-document',function(\App\Models\ApplicationForm $form){
//            return Storage::disk('public')->download($form->guidance_document,Str::slug($form->name." "."Guidance Document").".pdf");
//        })->name('applications.guidance');
//
//        Route::get('apply/{form}/privacy-policy',function(\App\Models\ApplicationForm $form){
//            return view('applications.privacy.index',[
//                'applicationForm' => $form,
//            ]);
//        })->name('applications.privacy');

        Route::get('apply/{form}/data-usage-statement',function(\App\Models\ApplicationForm $form){
            return view('applications.data-usage-statement.index',[
                'applicationForm' => $form,
            ]);
        })->name('applications.data');

//        Route::get('apply/{form}/terms',function(\App\Models\ApplicationForm $form){
//            return view('applications.terms.index',[
//                'applicationForm' => $form,
//            ]);
//        })->name('applications.terms');

        Route::get('apply/{form}/eligibility',function(\App\Models\ApplicationForm $form){
            return view('applications.eligibility.index',[
                'applicationForm' => $form,
            ]);
        })->middleware('applicationWindowClosed')->name('applications.eligibility');

        Route::get('apply/{form}/closed',function(\App\Models\ApplicationForm $form){
            return view('applications.closed',[
                'applicationForm' => $form,
            ]);
        })->name('applications.closed');

        Route::get('apply/{form}/application/{application}',function (\App\Models\ApplicationForm $form,\App\Models\Application $application){
            return view('applications.apply',[
                'application' => $application,
            ]);
        })->middleware('applicant')->name('applications.apply');
    });


});


